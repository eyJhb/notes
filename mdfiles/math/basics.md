# Basic math

# Fractions
Basic rules regarding fractions, that I sometimes forget and a easy place to link people I am helping.
A fraction is divided into a numerator and a denominator, which can be seen as the numerator showing e.g. how many slices of pizza we have, while the denominator shows how many are sharing it.

$$
\frac{numerator}{denominator}
$$

E.g. if we have 10 slices of pizza and 5 people sharing it, we have the fraction $\frac{10}{5}$, which means each will get $2$ slices each.

It is also important to remember, that if we have a negative sign before a fraction, it applies to the whole numerator.

$$
\frac{-2-x}{5} = -\frac{(2+x)}{5} = -\frac{2+x}{5}
$$

This is because of common mathematics rules, which implies there is a invisible parentheses around the numerator and denominator.
Keep that in mind throughout this text!

## Addition
This will both be for normal addition but also algebraic.

### Basic
The method for addition has the basic principles, that we want the denominators to be equal to each other.

$$
\frac{a}{b} + \frac{c}{d} 
$$

In the above example, we would like to have a common denominator ($b=d$), because if we have that, then we can add the numerator ($a+c$). 
This would then mean the following.

$$
\frac{a+c}{b}
$$

If instead there is not a common denominator, then we can either by visual inspection see what constant $c_1$ should be multiplied on $a$ and $b$, and what constant $c_2$ should be multiplied on $c$ and $d$.

$$
\frac{c_1 \cdot a}{c_1 \cdot b} + \frac{c_2 \cdot c}{c_2 \cdot d}
$$

See [Example - Basic 1](#addition-basic1) for a example on this.

If however it is not possible by visual inspection to find a common denominator, then it we can multiply use $d$ as our $c_1$ and $b$ as our $c_2$.

$$
\frac{d \cdot a}{d \cdot b} + \frac{b \cdot c}{b \cdot d}
$$

This will give us a common denominator, and a example of this can be seen in [Example - Basic 2](#addition-basic2).

The reason we are allowed to do this, will be explained in [Advanced Addition](#addition-advaned).

#### Example 1 {#addition-basic1}
If we have the following problem, the first step is to find a common denominator.

$$
\frac{1}{2} + \frac{3}{4} 
$$

It can be seen that if I multiple our first fraction with 2 in the numerator and denominator, that they will share denominator.

$$
\frac{2 \cdot 1}{2 \cdot 2} + \frac{3}{4} 
= \frac{2}{4} + \frac{3}{4} 
$$

Now because they share common denominator, it is a easy as just adding the two numerators together.

$$
\frac{2+3}{4}
= \frac{5}{4}
$$

#### Example 2 {#addition-basic2}
If we have the following problem, the first step is to find a common denominator.

$$
\frac{1}{3} + \frac{3}{8} 
$$

This is not as easy as the previous one, as finding a common denominator by inspection is somewhat harder.
Instead if we multiply the denominator of each fraction to the opposite fractions numerator and denominator, we will get a fraction with the common denominator that we want.

$$
\frac{8 \cdot 1}{8 \cdot 3} + \frac{3 \cdot 3}{3 \cdot 8} 
= \frac{8}{24} + \frac{9}{24} 
$$

Now we can do as previously and add our numerators together.

$$
\frac{8}{24} + \frac{9}{24} 
= \frac{8+9}{24}
= \frac{17}{24}
$$

### Advanced
The reason we are allowed to take any random number, and multiple our fractions with is because of the following.

$$
\frac{c}{c} = 1
$$

Because we have c in the numerator and denominator, it basically means $\frac{1}{1}$, which results in $1$, which then implies that we are multiplying our fraction by one.
This is very useful in general, when we want a common denominator.
Rewriting our previous expression, this gives us:

$$
\frac{d}{d}\frac{a}{b} + \frac{b}{b}\frac{c}{d}
$$

This will be used when we have algebraic expressions, where there will be given a example now.

#### Example 1
$$
\frac{3x+5}{8x}+\frac{x}{2x+8}
$$

Using the expression described before, we get:

$$
\frac{2x+8}{2x+8}\frac{3x+5}{8x}+\frac{8x}{8x}\frac{x}{2x+8}
= \frac{(2x+8)(3x+5)}{(2x+8)8x}+\frac{8x^2}{8x(2x+8)}
= \frac{6x^2+10x+24x+40}{16x^2+64x}+\frac{8x^2}{16x^2+64x}
= \frac{6x^2+34x+40}{16x^2+64x}+\frac{8x^2}{16x^2+64x}
$$

Now we have a common denominator, and can therefore just add our numerator together.

$$
\frac{6x^2+34x+40+8x^2}{16x^2+64x}
= \frac{14x^2+34x+40}{16x^2+64x}
= \frac{14x^2+34x+40}{16x^2+64x}
$$

This can further be reduced by dividing by two in the numerator and denominator.

$$
\frac{7x^2+17x+20}{8x^2+32x}
$$


## Subtraction
The rules for subtraction are the same as for addition, which means we want to find a common denominator and then subtract the two from each other.
We can show this using the same examples as before.

### Basic
#### Example 1
$$
\frac{1}{2} - \frac{3}{4} 
= \frac{2 \cdot 1}{2 \cdot 2} - \frac{3}{4} 
= \frac{2}{4} - \frac{3}{4} 
= \frac{2-3}{4}
= \frac{-1}{4}
= -\frac{1}{4}
$$

#### Example 2 
$$
\frac{1}{3} - \frac{3}{8} 
= \frac{8 \cdot 1}{8 \cdot 3} - \frac{3 \cdot 3}{3 \cdot 8} 
= \frac{8}{24} - \frac{9}{27} 
= \frac{-1}{24}
= -\frac{1}{24}
$$

### Advanced
#### Example 1
Using the example from addition right before we added the two fractions, lets use that and subtract instead.

$$
\frac{6x^2+34x+40}{16x^2+64x}-\frac{8x^2}{16x^2+64x}
= \frac{6x^2+34x+40-8x^2}{16x^2+64x}
= \frac{-2x^2+34x+40}{16x^2+64x}
= \frac{-x^2+17x+20}{8x^2+32x}
$$

## Fractions with fractions in numerator and denominator
If we use a, b, c and d as placeholders for our numerator and denominator, the method will look as follows.

$$
\frac{\frac{a}{b}}{\frac{c}{d}} = \frac{a}{b}\frac{d}{c} = \frac{ad}{bc}
$$

The two examples below should explain it fairly good!

### Example 1
There is no need to multiply the parentheses in the numerator, and is frowned upon in most cases.

$$
\frac{\frac{2+x}{2z}}{\frac{3z}{x-4}}
= \frac{2+x}{2z}\frac{x-4}{3z}
= \frac{(2+x)(x-4)}{2z \cdot 3z}
= \frac{2x-8+x^2-4x}{5z}
= \frac{x^2-2x-8}{6z^2}
$$

### Example 2
There is no need to multiply the parentheses in the numerator, and is frowned upon in most cases.

$$
\frac{\frac{8+x}{2x}}{\frac{8x}{x-4}}
= \frac{8+x}{2x}\frac{x-4}{8x}
= \frac{(8+x)(x-4)}{2x \cdot 8x}
= \frac{8x-32+x^2-4x}{16x^2}
= \frac{x^2-4x-32}{16x^2}
$$






