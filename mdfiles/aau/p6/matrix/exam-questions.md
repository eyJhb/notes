# Matrix Computations and Convex Optimization - Exam Questions

# Notes
## Unitary matrices
A matrix is unitary if its complex conjugated transpose $U^\*$ is also its inverse ($U^\*U=UU^\*=I$).
Remember that the identity matrix is a matrix where the diagonal is all ones.

The following holds for unitary matrices:

- $U^\* = U^{-1}$
- $U^\*U = UU^\* = I = 1$
- $(ABC)^\* = C^\*B^\*A^\*$

## Diagonal matrices
A diagonal matrix, is a matrix with only values in the diagonal, where the rest are 0.
E.g. the identity matrix is a diagonal matrix.
For a diagonal matrix, the following holds: $Diag^{-1} = Diag$.

A example could be:

$$
A = \begin{bmatrix}
1 & 0 & 0 \\\\
0 & 2 & 0 \\\\
0 & 0 & 3 \\\\
\end{bmatrix}
= \begin{bmatrix}
1 & 0 & 0 \\\\
0 & 2 & 0 \\\\
0 & 0 & 3 \\\\
\end{bmatrix}
$$

## Rank
A matrix is full row rank when each of the rows of the matrix are linearly independent and full column rank when each of the columns of the matrix are linearly independent. For a square matrix these two concepts are equivalent and we say the matrix is full rank if all rows and columns are linearly independent. A square matrix is full rank if and only if its determinant is nonzero.

For a non-square matrix with  rows and  columns, it will always be the case that either the rows or columns (whichever is larger in number) are linearly dependent. Hence when we say that a non-square matrix is full rank, we mean that the row and column rank are as high as possible, given the shape of the matrix. So if there are more rows than columns (), then the matrix is full rank if the matrix is full column rank.

https://www.cds.caltech.edu/~murray/amwiki/index.php/FAQ:_What_does_it_mean_for_a_non-square_matrix_to_be_full_rank%3F


# Question 1: Exercise 1.1 a and exercise 1.2 b

NEW NOTES:

- Write some of the unitary things - https://en.wikipedia.org/wiki/Unitary_matrix
    - https://en.wikipedia.org/wiki/Orthogonal_matrix
- https://www.youtube.com/watch?v=mBcLRGuAFUk

This should be all, to see that it holds. Make that clear!

## 1.1a) What is the relation between the SVD of a matrix *A* and the EVD of $AA^*$ and $A^*A$?
- Hint: Use the SVD of *A* in the expressions $AA^*$ and $A^*A$.

We have the following for SVD and EVD:

- $SVD(A) = U \Sigma V^\*$ (MM1S6)
    - Where U and V are unitary matrices (nxm) (see above for what unitary implies)
    - While $\Sigma$ is Diagonal
- $EVD(A) = X \wedge X^{-1}$ (MM1s15)
    - The columns of $X \in \mathbb{C}^{m \times m}$ consists of m linear independent eigenvectors
    - $\wedge = Diagonal( \lambda_1 \dots \lambda_m) \in \mathbb{C}^{m \times m}$ with $\lambda_i \in p(A)$

We will use the notes about unitary and diagonal matrices.

$$
\begin{align}
A^\*A &= (U \Sigma V^\*)^\*(U \Sigma V^\*) \\\\
&= V \Sigma^\* U^\*U \Sigma V^\* \text{$U^\*U = I$} \\\\
&= V \Sigma^\*\Sigma V^\* \\\\
&= V \Sigma^2 V^\* \\\\ 
\end{align}
$$

$$
\begin{align}
AA^\* &= (U \Sigma V^\*)(U \Sigma V^\*)^\* \\\\
&= U \Sigma V^\*V \Sigma^\* U^\* \\\\
&= U \Sigma \Sigma^\* U^\* \\\\
&= U \Sigma^2 U^\* \\\\
\end{align}
$$

The Sigma ($\Sigma$), being equal to the power of two, can easily be proven with knowing that $\Sigma^T = \Sigma$.

It can be seen in both our calculations, that we have $\Sigma^2$, which corresponds to the diagonal matrices of $A^\*A$ and $AA^\*$.
This is because that our $\Sigma^2$, are the eigenvalues of $A^\*A$, which is what $\lambda$ is for our EVD.
This the matches our expression for the EVD, given that we know that $V^\* = V^{-1}$.
Thereby we have show the relation between EVD and SVD, given our input values of A.

Maybe mention this? $ \pm \Sigma = \wedge$

## 1.2b) Given *A*, and $b = [6, 3, 9]^T$, solve $Ax=b$ for *x*.

$$
\begin{align}
A &= \begin{bmatrix}
1 & 2 & 3 \\\\
0 & 1 & 1 \\\\
1 & 2 & 0 \\\\
\end{bmatrix} \\\\
b &= \begin{bmatrix}
6 \\\\
3 \\\\
9 \\\\
\end{bmatrix} 
\end{align}
$$

We need to do this using the SVD, which gives us the formula:

$$
\begin{align}
U\Sigma V^\* x = b \\\\
x = V\Sigma^{-1} U^\* b \\\\
\end{align}
$$

This can then be solved using Python like below (also show using inv):

```python
import numpy as np
A = np.array([[1,2,3],[0,1,1],[1,2,0]])
b = np.array([6, 3, 9]).T

# for SVD
u, d, v = np.linalg.svd(A)
S = np.zeros((3, 3))
np.fill_diagonal(S, d)

S = np.diag(1/np.diag(S))

x = v.conj().T @ S @ u.conj().T @ b

# for inv
x = np.linalg.inv(A) @ b
> array([ 1.,  4., -1.]) (flip this to get a column instead)
```

We can also do this by hand, using row reduction, so that $[ A | I ] \rightarrow [ I | A^{-1} ]$

![by hand](figures/q1-12b.png)

This can be done in Python using inverted of A ($x=A^{-1}b$), or the SVD of A ($A=U \Sigma V^\*$, $x=V \Sigma^{-1} U^\*b$).

Verify the results against a direct computation of the SVD of A.

# Question 2: Exercise 2.2 a and b
Define a matrix and a vector as

$$
\begin{align}
A &= \begin{bmatrix}
1 & 0 \\\\
2 & 1 \\\\
3 & 1 \\\\
\end{bmatrix} \\\\
b &= \begin{bmatrix}
6 \\\\
3 \\\\
9 \\\\
\end{bmatrix}
\end{align}
$$

## a)
- Find the orthogonal projection matrix for the space spanned by the columns of a full column rank matrix. Use this to find the orthogonal projection matrix for the space spanned by the columns of A.

![graph](figures/q3-a.png)

See below for proof

$$
\begin{align}
r &= b - Ax \\\\
A^\* r &= 0 \\\\
A^\* (b - Ax) &= 0 \\\\
A^\* b - A^\* Ax &= 0 \\\\
A^\* Ax &= A^\* b \\\\
x &= (A^\* A)^{-1} A^\* b \\\\
Ax &= A(A^\* A)^{-1} A^\* b \\\\
\end{align}
$$

For a projection matrix, the following holds:

- Projection Matrix: $P^2 = P$
- Orthogonal Projection Matrix: $P^\* = P$

The orthogonal projector onto range(A) is given by: $Proj_v \vec{x} = A(A^\*A)^{-1}A^\*$ (Trefethen p. 46).
This can be calculated in Python:

Keep in mind, our span of subspace V is given as the columns in our matrix A, which are linear independent (they are a basis for V).

```python
import numpy as np
A = np.array([[1,0],[2,1],[3,1]])
b = np.array([6,3,9]).T

P = A @ np.linalg.inv(A.conj().T @ A) @ A.conj().T
P = np.dot(np.dot(A, np.linalg.inv(np.dot(A.conj().T, A))),A.conj().T)
>>> P
array([[ 0.66666667, -0.33333333,  0.33333333],
       [-0.33333333,  0.66666667,  0.33333333],
       [ 0.33333333,  0.33333333,  0.66666667]])
```

This can also be done by hand, but is cumbersome, therefore the python result is used.

$$
P = \frac{1}{3} \cdot \begin{bmatrix}
2  & -1 & 1 \\\\
-1 & 2  & 1 \\\\
1  & 1  & 2 \\\\
\end{bmatrix}
$$

## b)
- Make an orthogonal projection of the vector b onto the column space of A and explain the result.
- Hint: First, find *Pb*. Second, find the residual $r=b−Pb$ and explain.

Finding *Pb* first and then finding *r* can be seen in the Python code below.

```python
import numpy as np
A = np.array([[1,0],[2,1],[3,1]])
b = np.array([6,3,9]).T

P = A @ np.linalg.inv(A.conj().T @ A) @ A.conj().T
Pb = P @ b
> array([6., 3., 9.])
r = b - Pb
> array([ 2.66453526e-15, -7.99360578e-15, -1.24344979e-14])
```

The orthogonal projection is $Pb=b$ and the residual $r=b−Pb=0$, therefore b is in the column space of A.

This can also be seen as b is in the range of A, because b is a linear combination of A ($a_1\vec{v}_1 + a_2\vec{v}_2 + \dots + a_n\vec{v}_n$).

$$
6 \cdot \begin{bmatrix}
1 \\\\
2 \\\\
3 \\\\
\end{bmatrix}
+ (-9) \cdot \begin{bmatrix}
0 \\\\
1 \\\\
1 \\\\
\end{bmatrix}
= \begin{bmatrix}
6 \\\\
12 \\\\
18 \\\\
\end{bmatrix}
- \begin{bmatrix}
0 \\\\
9 \\\\
9 \\\\
\end{bmatrix}
= \begin{bmatrix}
6 \\\\
3 \\\\
9 \\\\
\end{bmatrix}
$$

# Question 3: Exercise 3.1 b
We are given matrix A

$$
A = \begin{bmatrix}
1 & 0 \\\\
2 & 1 \\\\
3 & 1 \\\\
\end{bmatrix}
$$

## b)
-  Is the matrix B symmetric, and is it positive definite? Try to do (a) and (b) without using the numerical value of A.

We let matrix B be defined as $B = A^\*A$.
Which is defined as below, from previous question.

$$
B = \begin{bmatrix}
    14 & 5 \\\\
    5  & 2 \\\\
\end{bmatrix}
$$

$$
\begin{align}
B &= A^\*A \\\\
B^\* &= (A^\*A)^\* = A^\*A = B \\\\
\end{align}
$$

A $n \times n$ matrix is said to be positive definite, if  $x^\*Bx$ is positive for every non-zero column vector x ($x^\*Bx > 0$ and $x \neq 0$).

$$
\begin{align}
x^\*Bx = x^\*A^\*Ax = (xA)^\*Ax 
\end{align}
$$

Because A has full rank (see rank above), the only way $Ax$ can give 0, only when x is zero, which it cannot be.
It can however also be shown like below (note we can use the norm 2, because x is a matrix therefore it will become a vector when Ax is multiplied):

$$
x^\*Bx = x^\*A^\*Ax = (Ax)^\*(Ax) = || Ax ||_2^2
$$

We can also determine it using the eigenvalues, which can be computed in Python, the rules are as follows.

1. M is positive definite if and only if all of its eigenvalues are positive.
2. M is positive semi-definite if and only if all of its eigenvalues are non-negative.
3. M is negative definite if and only if all of its eigenvalues are negative
4. M is negative semi-definite if and only if all of its eigenvalues are non-positive.
5. M is indefinite if and only if it has both positive and negative eigenvalues.

```python
import numpy as np
A = np.array([[1,0],[2,1],[3,1]])
B = A.conj().T @ A
np.linalg.eigvals(B)
> array([15.81024968,  0.18975032])
```

Therefore the eigenvalues are positive, the first rule applies and it in positive definite.

We can also quickly calculate this by hand.

$$
\begin{align}
(A-\lambda I) = 0 \\\\
\begin{bmatrix}
    14 & 5 \\\\
    5  & 2 \\\\
\end{bmatrix}
- \lambda
\begin{bmatrix}
    1 & 0 \\\\
    0 & 1 \\\\
\end{bmatrix}
= 0 \\\\
det(\begin{bmatrix}
    14 - \lambda & 5 \\\\
    5  & 2 - \lambda \\\\
\end{bmatrix}) = 0 \\\\
(14 - \lambda) \cdot (2 - \lambda) - 5 \cdot 5 = 0 \\\\
\lambda^2 - 16 \lambda + 3 = 0 \\\\
\lambda = \frac{-(-16) \pm \sqrt{(-16)^2 - 4 \cdot 1 \cdot 3}}{2 \cdot 1} \\\\
\lambda = \frac{16 \pm \sqrt{244}}{2} \\\\
\text{rewrite 2, to simplify our square root above} \\\\
\lambda = \frac{16 \pm \sqrt{244}}{\sqrt{4}} \\\\
\lambda = 8 \pm \sqrt{61} \\\\
\end{align}
$$

Shows the same as above!


# Question 4: Exercise 4.1 c

$$
A = \begin{bmatrix}
4 & 0 \\\\
0 & 2 \\\\
1 & 1 \\\\
\end{bmatrix}
, B = \begin{bmatrix}
2 \\\\
0 \\\\
11 \\\\
\end{bmatrix}
$$

## c)
- Show that the residual vector r is orthogonal to the columns of A and verify it by using the values in Eq. (1) (that is the A and B matrices).
- Hint: if two vectors are orthogonal, then $x^\*y=0$. In this case, the residual vector should be orthogonal to all the columns of A, i.e., $A^T r=0$

Besides this hint, we know that

- $r = b-Pb$
- $P=A(A^TA)^{-1}A^T$
- $A^TA(A^TA)^{-1} = I = 1$ (matrix times matrix inverted = I)

Then we can do the following calculations.

$$
\begin{align}
A^\* r &= 0 \\\\
A^\* (b - A(A^\* A)^{-1} A^\* b) &= 0 \\\\
A^\*b - A^\*A(A^\* A)^{-1} A^\* b) &= 0 \\\\
A^\*b - A^\* b &= 0 \\\\
\end{align}
$$


Python below does not work (maybe it does now?):

```python
import numpy as np
A = np.array([[4,0],[0,2],[1,1]])
b = np.array([2,0,11]).T
x = np.array([1,2]).T

r = b-A @ x
> array([-2, -4,  8])

A.conj().T @ r
> [0, 0]
```

# Question 5: Exercise 6.1 a and b
A constrained optimisation problem is given by

$$
\begin{align}
    \text{minimise}   & & f(x_1, x_2) &= x_1^2 + x_2 + 4 \\\\
    \text{subject to} & & c_1(x_1, x_2) &= -x_1^2 - (x_2 + 4)^2 + 16 \geq 0 \\\\
                      & & c_2(x_1, x_2) &= x_1 - x_2 - 6 \geq 0 
\end{align}
$$

## a)
- Using a graphical method, indicate the feasible region and solve the minimisation problem.

Firstly it is possible to check if the function in convex, by computing the hessian matrix, and then checking if the eigenvalues if that is positive (it needs to be positive semi-definite).
For this it would be...

$$
\begin{align}
H = \begin{bmatrix}
2 & 0 \\
0 & 0 \\
\end{bmatrix}
\end{align}
$$

Where we have $\lambda_1 + \lambda_2 = spor$ (diagonal added together), and $\lambda_1 \cdot \lambda_2 = determinant$ ($2 \cdot 0 + 0 \cdot 0 = 0$), which gives us $\lambda_1 = 2$ and $\lambda_2 = 0$, which shows it is a convex function.
A concave function is concave if $-f(x)$ is convex.
Should also be able to check with cholesky, but not sure how that works 100%.

Looking at our constraints it can be seen that  our $c_1$ looks like the equation for a circle $r^2=(x-a)^2+(y-b)^2$.
We can the rewrite it is $c_1(x_1, x_2) = (x_1-0)^2 + (x_2 + 4)^2 \geq 4^2$, which gives us a circle with center in $(0,-4)$ and radius $r=4$.
$c_2$ is a straight line with that intersect the y axis in $y=6$, with $a=1$ ($x_2 \leq x_1 - 6$).

This means that our function that needs to be optimized, can only take on values is-between the line and the circle (bottom) as shown in the figure.

![graph](figures/q5-61a.png)

Finally we have the function that needs to be minimised, $f(x_1, x_2) = x_1^2 + x_2 + 4$, we can make it into a "2d" figure by setting one variable static, and in our case $f(x_1, x_2)$ is chosen as we want to minimise the function so that it barely touches the bottom of our circle.
Therefore we get the expression: $x_2 = - x_1^2 - 4 + f(x_1, x_2)$, which indicates that is a downward parabola (descending), which intersects in -4.
As we have a circle with center in $(0, -4)$ and r=4, we want our parabola to insersect so that $f(x_1, x_2) = -8$, this can be done by setting $f(x_1, x_2) = -4 \rightarrow x_2 = -x_1^2 - 8$.


Helps understanding it - https://www.khanacademy.org/math/multivariable-calculus/applications-of-multivariable-derivatives/lagrange-multipliers-and-constrained-optimization/v/constrained-optimization-introduction

## b)
- Is the solution to the constrained optimisation problem the same as the solution to the optimisation problem without the constraints?

No. We can minimise f(x) as much as we want by decreasing the value of $x_2$.
This minimum is outside the feasible region of the constrained optimisation problem.

This is before the function can be optimization indefinitely, while it will just be outside of our constraints in general.

# Question 6: Exercise 6.3 a and b


## a)
- Find the feasible directions at $x_a$

Point $x_a = \begin{bmatrix}2 & 4\end{bmatrix}^T$ is a possible minimiser to the optimisation problem given by

$$
\begin{align}
    \text{minimise}   & & f(x_1, x_2) &= \frac{1}{4} \left[ x_1^2 + 4x_2^2 - 4(3x_1 + 8x_2) + 100 \right] \\\\
    \text{subject to} & & x_1 = 2 \\\\
                      & & x_2 \geq 0
\end{align}
$$

From the constraints we have that $x_1 = 2$, which gives us a vertical line that is parallel with the y-axis and intersects in x=2.
This means that we are constrained to $x_1 = 2$ as long as $x_2 \geq 0$, which is our feasible region.
Since a non-zero stop in a feasible direction *d* must also be in our feasible region, the feasible directions for $x_a$ can be written as $d = \begin{bmatrix} 0 \\\\ d_2 \end{bmatrix}$.

OLD BELOW:

This can be described by having $\alpha \cdot d = \begin{bmatrix}0 & 1\end{bmatrix}^T$, where $\alpha$ is either 1 or -1, depending if we want to go up or down (until 0).

![graph](figures/q6-1.png)

## b)
- Check if the second-order necessary conditions are satisfied

$$
\begin{align}
g(x^\*)^Td \geq 0 \text{If this is zero, the above is $>0$, no need to check} \\\\
d^T H(x^\*) d  \geq 0 \\\\
\end{align}
$$

This can be done by calculating the Hessian matrix of $f$, which is defined by.

$$
H_{i,j} = \frac{\partial^2 f}{\partial x_i \partial x_j}
$$

$$
H = \begin{bmatrix}
  \dfrac{\partial^2 f}{\partial x_1^2} & \dfrac{\partial^2 f}{\partial x_1\,\partial x_2} & \cdots & \dfrac{\partial^2 f}{\partial x_1\,\partial x_n} \\\\[2.2ex]
  \dfrac{\partial^2 f}{\partial x_2\,\partial x_1} & \dfrac{\partial^2 f}{\partial x_2^2} & \cdots & \dfrac{\partial^2 f}{\partial x_2\,\partial x_n} \\\\[2.2ex]
  \vdots & \vdots & \ddots & \vdots \\\\[2.2ex]
  \dfrac{\partial^2 f}{\partial x_n\,\partial x_1} & \dfrac{\partial^2 f}{\partial x_n\,\partial x_2} & \cdots & \dfrac{\partial^2 f}{\partial x_n^2}
\end{bmatrix}
$$


First lets find the gradient of $f$, to make it easier.

$$
g(x) = \begin{bmatrix}
    \frac{\partial}{\partial x_1} = \frac{1}{2}x_1 - 3 \\\\
    \frac{\partial}{\partial x_2} = 2x_2 - 8 \\\\
\end{bmatrix}
$$

Now the Hessian matrix can be calculated.
The Hessian matrix will be a square 2x2 matrix, as we have $x_1$ and $x_2$, where we need to take the $\partial^2$ in regards to :

$$
H = \begin{bmatrix}
    \frac{1}{2} & 0 \\\\
    0 & 2 \\\\
\end{bmatrix}
$$

$$
\begin{align}
g(x_a)^Td &= \begin{bmatrix}-2 & 0\end{bmatrix} \begin{bmatrix} 0 \\\\ 1 \end{bmatrix} = -2 \cdot 0 + 0 \cdot 1 = 0 \\\\
d^T H(X_a) d  &= \begin{bmatrix}
0 & 1
\end{bmatrix}
\begin{bmatrix}
\frac{1}{2} & 0 \\\\
0 & 2
\end{bmatrix}
\begin{bmatrix}
0 \\\\
1
\end{bmatrix}
= \begin{bmatrix}
0 \\\\
2
\end{bmatrix}
\begin{bmatrix}
0 \\\\
1
\end{bmatrix}
= 2 
\end{align}
$$

Because of $g(x_a)^Td=0$, then we actually do not need $d^T H(X_a) d = \geq 0$, as we know they will be above 0, the second order conditions are satisfied for the feasible directions at $x_a$.
If our first equation was $\geq 0$, then we would not need to do the second calculation.

Helps regarding calculating the Hessian matrix - https://www.youtube.com/watch?v=qF0aDJfEa4Y

# Question 7: Exercise 8.1 e
TODO(eyJhb) fix constraints and use g/h for constraints

We have the following from the start of the assignment

$$
\begin{align}
    \text{minimise}   & & f(x_1, x_2) &= x_1^2 + x_2^2 + 5 \\\\
    \text{subject to} & & a_i(x_1, x_2) &= 0 \quad i=1, \dots, p \\\\
                      & & c_i(x_1, x_2) &\geq 0 \quad i=1, \dots, q \\\\
\end{align}
$$

With the following additional info in 8.1e (I think there is a error in the given assignment, and $c_2$ should be $c_1$, which is noted below).

$$
\begin{align}
    p &= q = 1 \\\\
    a_1(x) &= x_1 - x_2 + 2 \\\\
    c_2(x) = c_1(x) &= 3x_1 + 3x_2 + 3
\end{align}
$$

Assignment: Use Lagrange multipliers to determine all regular minimizers.


The above means that we have a our two constraints, where we have the constraints as listed above, for $c_i(x_1, x_2) \geq 0$ and $a_i(x_1, x_2) = 0$.
What we want to find here, is where the gradients are perpendicular to each other (point where our constraints tangent with our f function, so that they all form a gradient that is equal to 0).
This can be written as:

$$
\begin{align}
    \nabla f(x) = \lambda \nabla a_1 = \mu \nabla c_2 \\\\
    \nabla f(x) - \lambda \nabla a_1 - \mu \nabla c_2 = 0 \\\\
\end{align}
$$

This is because, they might not have the same length, therefore we add the Lagrange multiplier ($\lambda$ and $\mu$ in this case, could have been $\lambda_n$ instead).

We can rewrite our equations $a_1$ and $c_1$ as follows, to make it easier doing our calculations.

$$
\begin{align}
    a_1(x) &= Ax + b = \begin{bmatrix}1 & -1\end{bmatrix}x + 2 \\\\
    c_1(x) &= Cx + b = \begin{bmatrix}3 & 3\end{bmatrix}x + 3
\end{align}
$$

The equations/inequalities to be solved are:

$$
\begin{align}
\nabla f(x) - A^T\lambda - C^T\mu = 0 \\\\
Ax=b \\\\
c_1(x)\mu = 0 \\\\
c_1(x) \geq 0 \\\\
\mu \geq 0
\end{align}
$$

If we take the gradient of these ($\frac{\partial}{\partial x}$) of these, then we just end up with our matrices, since our $x_1$ and $x_2$ are either constants or to the power of one.
To make this clear, it would look like (can also be seen from the base statement $Ax + b$):

$$
\begin{align}
    \nabla a_1(x) &= A = \begin{bmatrix}1 & -1\end{bmatrix} \\\\
    \nabla c_1(x) &= C = \begin{bmatrix}3 & 3\end{bmatrix}
\end{align}
$$

This then gives us our expression from above as: 

$$
\begin{align}
    \nabla f(x) - \nabla a_1(x) \lambda - \nabla c_2(x) \mu = 0 \\\\
    \nabla f(x) - \lambda A^T - \mu C^T = 0 \\\\
\end{align}
$$

Finding the gradient for $f(x)$ is as simple as:

$$
\begin{align}
\nabla f(x) = 
\begin{bmatrix}
    \frac{\partial f(x)}{\partial x_1} \\\\
    \frac{\partial f(x)}{\partial x_2} \\\\
\end{bmatrix}
= \begin{bmatrix}
    2x_1 \\\\
    2x_2 
\end{bmatrix}
= \begin{bmatrix}
    2 \\\\
    2 
\end{bmatrix}
x
\end{align}
$$

This yields in the following matrix

$$
\begin{align}
\begin{bmatrix}
    2 \\\\
    2 \\\\
\end{bmatrix}
x
- \begin{bmatrix}
    1 \\\\
    -1 \\\\
\end{bmatrix} \lambda
- \begin{bmatrix}
    3 \\\\
    3 \\\\
\end{bmatrix} \mu
= 0 \\\\
\end{align}
$$

This gives us two equations with four unknowns.
This can be solved, by using the two equations combined with our constraints above, which gives us the following matrix which can be solved in a CAS program or using reduced echelon form.

$$
\begin{align}
\begin{bmatrix}
    2 & 0  & -1 & -3 \\\\
    0 & 2  & 1  & -3 \\\\
    1 & -1 & 0  & 0  \\\\
    3 & 3  & 0  & 0
\end{bmatrix}
\begin{bmatrix}
x_1 \\\\
x_2 \\\\
\lambda \\\\
\mu 
\end{bmatrix} &= 
\begin{bmatrix}
0 \\\\
0 \\\\
-2 \\\\
-3  
\end{bmatrix} \\\\
3x_1 + 3x_2 + 3 & \geq 0 \\\\
\mu & \geq 0
\end{align} 
$$

Which yields the results:

$$
\begin{align}
x_1 &= -\frac{3}{2} \\\\
x_2 &= \frac{1}{2} \\\\
\lambda &= -2 \\\\
\mu &= -\frac{1}{3}
\end{align} 
$$


This cannot be a solution, as $\mu \geq 0$ (from our constraints), another solution can be found using Matlab, WolframAlpha etc. given as $x_1 = -1$, $x_2 = 1$, $\lambda = -2$, $\mu = 0$, which is a valid solution.

# Question 8: Exercise 9.1 c
Consider the 3 dimensional LP problem

$$
\begin{align}
    \text{minimise}   & & f(x_1, x_2) &= -x_1 - x_2 \\\\
    \text{subject to} & & x_1 - x_2 &= 0 \\\\
                      & & x & \geq_e 0 \qquad \text{Where $\geq_e$ means element wise}
\end{align}
$$

## c)
- Try to solve the LP (linear programming) problem by means of the KKT condition. Does there exists a minimizer?
- Linear Programming Problem is one that is concerned with finding the optimal value (maximum or minimum value) of a linear function (called objective function) of several variables (say x and y)

We can write the problem as

$$
\begin{align}
    \text{minimise}   & & f(x) &= \begin{bmatrix} -1 & -1 \end{bmatrix}x \\\\
    \text{subject to} & & h_1(x) = Ax = b & \Rightarrow \begin{bmatrix} 1 & -1 \end{bmatrix}x = 0 \\\\
                      & & g_1(x) = x & \geq_e 0 
\end{align}
$$

In the above f is the objective or utility function, $g_i(i = 1, \dots , m)$ are the inequality constraint functions and $h_j(j = 1, \dots l)$ are the equality constraints.
The necessary conditions for minimizing are as follows:


TODO(eyJhb) Fix below?

$$
\begin{align}
f(x): \nabla f(x^\*) + \sum_{i=1}^m \mu_i \nabla g_i (x^\*) + \sum_{i=1}^m \mu_i \nabla g_i (x^\*) 
\end{align}
$$

The KKT conditions are as follows: 

$$
\begin{align}
A^T \lambda + \mu &= c \\\\
Ax &= b \\\\
X\mu &= 0, \\; X = diag(x_1, x_2) \\\\
x &\geq_e 0 \\\\
\mu &\geq_e 0
\end{align}
$$

The first expression gives us

$$
\begin{align}
\begin{bmatrix}
 1 \\\\
 -1
\end{bmatrix}
\lambda + \mu = 0 \\\\
\begin{bmatrix}
\lambda + \mu_1 = 0 \\\\
-\lambda + \mu_2 = 0 
\end{bmatrix}
\end{align}
$$

Which is not possible, as it implies that $\lambda \geq 1$ and $\lambda \leq 1$, as $\mu$ cannot be less than 0.

# Notes from exam
Got question 8

- What does it mean to minimize the functions
- Is the $x \geq_e 0$ a basic constraint
- How do you get the KKT conditions for that function
- What does it mean for problem to be a LS problem?
    - Do the constraints needed to be linear as well?
- What does it mean for something to be convex
- What group does LP problems belong to?
