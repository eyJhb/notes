# Exam Plan

## Probability Theory and Statistics
- pmm1 - Elements of probability
- pmm2 - Random variables
- pmm3 - Expectation and variance
- pmm4 - Special random variables (examples of distributions)
- pmm5 - Special random variables (examples of distributions)
- pmm6 - Workshop 1
- pmm7 - Multidimensional random variables
- smm1 - Presentation of the statistics part of the course
- smm2 - Sampling Distributions. Introduction to Detection and Parameter Estimation
- smm3 - Parameter Estimation and Confidence Intervals
- smm4 - Hypothesis Testing, Part I
- smm5 - Hypothesis Testing, Part II
- smm6 - Regression
- smm7 - Workshop

## Matrix Computations and Convex Optimization
- mmm1 - Singular Value Decomposition and Eigenvalues
- mmm2 - Factorization
- mmm3 - Stability and Factorization
- mmm4 - Least Squares
- mmm5 - Exercise Session connected to the matrix computation part
- mmm6 - Optimization: Basic Principles
- mmm7 - Basic Multidimensional Gradient Methods
- mmm8 - Fundamentals of Constrained Optimization
- mmm9 - Linear Programming: Interior-Point Methods
- mmm10 - Exercise Session connected to the convex optimization part:
- mmm11 - Workshop

| Date  | Done | What to do                    | Notes |
|-------|------|-------------------------------|-------|
| 28-04 |      | pmm1                          |       |
| 29-04 |      | pmm2                          |       |
| 30-04 |      | pmm3                          |       |
| 01-05 |      | pmm4                          |       |
| 02-05 |      | Weekend - Catchup             |       |
| 03-05 |      | Weekend - Catchup             |       |
| 04-05 |      | pmm5                          |       |
| 05-05 |      | pmm6                          |       |
| 06-05 |      | pmm7                          |       |
| 07-05 |      | smm1                          |       |
| 08-05 |      | smm2                          |       |
| 09-05 |      | Weekend - Catchup             |       |
| 10-05 |      | Weekend - Catchup             |       |
| 11-05 |      | smm3                          |       |
| 12-05 |      | smm4                          |       |
| 13-05 |      | smm5                          |       |
| 14-05 |      | smm6                          |       |
| 15-05 |      | smm7                          |       |
| 16-05 |      | Weekend - Catchup             |       |
| 17-05 |      | Weekend - Catchup             |       |
| 18-05 |      | mmm1                          |       |
| 19-05 |      | mmm2                          |       |
| 20-05 |      | mmm3                          |       |
| 21-05 |      | mmm4                          |       |
| 22-05 |      | mmm5                          |       |
| 23-05 |      | Weekend - Catchup             |       |
| 24-05 |      | Weekend - Catchup             |       |
| 25-05 |      | mmm6                          |       |
| 26-05 |      | mmm7                          |       |
| 27-05 |      | mmm8                          |       |
| 28-05 |      | mmm9                          |       |
| 29-05 |      | mmm10                         |       |
| 30-05 |      | mmm11                         |       |
| 31-05 |      | Summarise prop/stat till exam |       |
| 01-06 |      |                               |       |
| 01-06 |      |                               |       |
| 02-06 |      |                               |       |
| 03-06 |      |                               |       |
| 04-06 |      |                               |       |
| 05-06 |      |                               |       |
| 06-06 |      |                               |       |
| 07-06 |      |                               |       |
| 08-06 |      | Propability/Statistics Exam   |       |
| 09-06 |      |                               |       |
| 10-06 |      | matrix and convex exam        |       |
| 11-06 |      | matrix and convex exam        |       |
| 12-06 |      | matrix and convex exam        |       |

Dates for exam found [here](https://www.es.aau.dk/education/SPRING+SEMESTER/#448148).

<!-- generate-tec -->
