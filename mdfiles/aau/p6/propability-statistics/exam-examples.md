# Probability 2015
## Problem 1
Suppose that X and Y are two discrete random variables whose joint probability mass function is given as following

- P(X = −1, Y = −1) = 1/4; P(X = 0, Y = −1) = 1/4; P(X = 1, Y = −1) = 0; P(X = −1, Y = 1) = 0; P(X = 0, Y = 1) = 1/4; P(X = 1, Y = 1) = 1/4

| Y/X |        |     | X   |     |     |
|-----|--------|-----|-----|-----|-----|
|     | f(x,y) | -1  | 0   | 1   |     |
| Y   | -1     | 1/4 | 1/4 | 0   | 1/2 |
|     | 1      | 0   | 1/4 | 1/4 | 1/2 |
|     |        | 1/4 | 1/2 | 1/4 |     |

### A)
- Find marginal distributions for X and Y

Found above

$$
\begin{align}
P(X=-1) = 1/4 \\\\
P(X=0) = 1/2 \\\\
P(X=1) = 1/4 \\\\
P(Y=-1) = 1/2 \\\\
P(Y=1) = 1/2 \\\\
\end{align}
$$

### B)
- Are these random variables independent? In case they are not independent, suggest new random variables X1 and Y1 such that the new random variables have the same marginal distributions as the old ones and are independent.

If they are independent, the following should hold: $P(X=i,Y=j) = P(X=i) \cdot P(Y=j)$.

For $i=1$, $j=1$, $P(X=1,Y=1)=1/4$, $P(X=1) \cdot P(Y=1) = 1/4 \cdot 1/2 = 1/8$, not independent.
Finding new values, where this holds.

| Y/X |        |     | X   |     |     |
|-----|--------|-----|-----|-----|-----|
|     | f(x,y) | -1  | 0   | 1   |     |
| Y   | -1     | 1/8 | 1/4 | 1/8 | 1/2 |
|     | 1      | 1/8 | 1/4 | 1/8 | 1/2 |
|     |        | 1/4 | 1/2 | 1/4 |     |

### C) Calculate the expected values for X and Y

$$
\begin{align}
E[X] = \sum\_i iP(x=i) = -1 \cdot 1/4 + 1 \cdot 1/4 = 0 \\\\
E[Y] = \sum\_j iP(Y=j) = -1 \cdot 1/2 + 1 \cdot 1/2 = 0 \\\\
\end{align}
$$

# Statistics <unknown>

## Problem 1
A student is taking a multiple choice test where each question has four options for an answer.
The student mastered mastered 70% of the material.
Assume this means that the student has a 0.7 chance of knowing the correct answer to a random test question.
On the other hand, if the student does not know the answer to the question, she randomly selects among the four answer choices.
Finally, assume that this holds for each question independent of the others.

### a)
- What is the probability that a specific question is answered correctly?

We always have a 70% chance (0.7), that means that there is 30% left, where these are divided between 4 answer where one is correct.
That means there is $0.25 \cdot 0.3$ chance of getting it correct, plus the initial chance.

```python
Pcorr = 0.7 + 0.25 * 0.3
> 0.775
```

### a)
- If the test has 30 questions, what is the expected number of correct choices? Suppose that at least 90% of the choices has to be correct to pass the test.

This can be done by using the following formula.

$$
E \left \sum\_{i=1}^{30} P\_corr \right]
$$

```python
30 * Pcorr
> 23.25
```

### c)
- If the test has 30 questions, approximate the probability that the student will get at least 90% of the choices correct?

This can be done using the method from the solutions, and using Python for the exact solution.

```python
from scipy.stats import binom
Pcorr = 0.7+0.25*0.3
1-binom.cdf((0.9*30)-1, 30, Pcorr)
> 0.06959601794094872
```

### d)
- The teacher allows the student to choose between two tests: one with 30 questions and one with 100 questions. Which should the student choose?

30 questions. By the law of large numbers, the variance decreases with more questions.

This can be tested if one wants to do so.

## Problem 2

```python-test
from sklearn.linear_model import LinearRegression
import numpy as np
n=10
R=np.arange(1,n+1)
V=np.array([13.45,16.01,8.57,15.55,8.17,8.18,10.27,10.88,6.21,5.41])

y=np.log(V).conj().T
x=np.append([np.ones(10).T], [R.T])
B = LinearRegression().fit(x,y)
exit()
#B=regress(y,x);
#
#a=B(1);
#b=B(2);
#%% (a) find A and q
#A=exp(a)
#q=1-exp(b)
#
#hold on
#plot(R,A*(1-q).^R,'r')
#
#%(b) find the 90% prediction interval for R=15
#x0=15;
#meanx=mean(x(:,2));
#Sxx=sum((x(:,2)-meanx).^2);
#Sxy=B(2)*Sxx;
#meany=mean(y);
#Syy=sum((y-meany).^2);
#SSR=(Sxx*Syy-(Sxy)^2)/Sxx;
#sigma2=SSR/(n-2);
#Spred=sqrt(1+1/n+(x0-meanx)^2/Sxx)*sqrt(SSR/(n-2))*tinv(0.95,n-2);
#
#UPlog=B(1)+B(2)*x0+Spred;
#LOWlog=B(1)+B(2)*x0-Spred;
#
#UP=exp(UPlog)
#LOW=exp(LOWlog)
#
#%% (c) Test the hypothesis that V decreases with R at 5% significance level.
#TS = sqrt((n-2)*Sxx/SSR)*abs(b)
#
#p_val=2*(1-tcdf(TS,n-2))
```

## Problem 3
A study was instituted to learn how the diets of women changed during the winter and the summer.
A random group of 12 women were observed during the month of July and the percentage of each woman’s calories that came from fat was determined.
Similar observations were made on a different randomly selected group of size 12 during the month of January.
The results were as follows:

|---------|------------------------------------------------------------------------|
| July    | 29.1, 29.4, 36.4, 36.0, 32.8, 36.0, 24.8, 33.0, 37.0, 32.1, 34.4, 33.1 |
| January | 43.0, 33.7, 34.0, 35.2, 25.4, 45.6, 40.4, 35.8, 45.3, 31.1, 38.9, 37.9 |
|---------|------------------------------------------------------------------------|

Test the hypothesis that the mean fat percentage intake is the same for both months.
Use the (a) 5 percent level of significance and (b) 1 percent level of significance.

For this we will use the paired t-test, which can be calulated using the notes from SMM5S9.
The below will do it for us.

```python
from scipy.stats import t
import numpy as np
from math import *

jun = np.array([29.1, 29.4, 36.4, 36.0, 32.8, 36.0, 24.8, 33.0, 37.0, 32.1, 34.4, 33.1])
jan = np.array([43.0, 33.7, 34.0, 35.2, 25.4, 45.6, 40.4, 35.8, 45.3, 31.1, 38.9, 37.9])
n = len(jun)
 
x = jun-jan
xbar = np.mean(x)
S=sqrt(sum(np.power(x-xbar,2))/(n-1))
TS=sqrt(n)/S*xbar
pvalue=2*t.cdf(TS,n-1)
(TS, pvalue)

> (-2.2298808850633884, 0.04753626645080209)
````

# Statistics 2016
## Problem 4

### a)

```python
import statistics
z = statistics.NormalDist().inv_cdf(1-(0.05/2))
p = 260/1200
> 0.21666666666666667
q = 1-p
E = z*sqrt((p*q)/1200)
> 0.02330917072068709
```

Result $p - E < p < p + E$

# Statistics 2018
## Problem 2
### A)

```python
import statistics
import numpy as np
belt = np.array([5.1, 5.9, 7.2, 7.9, 7.8, 5.4, 6.7, 6.8, 7.9, 5.7])
z = statistics.NormalDist().inv_cdf(1-(0.10/2))
belt.mean()-z*(belt.std()/sqrt(len(belt))
> 6.114572884163848
```

### B)

$$
0,01 > z\_{0.025} \sqrt{\frac{pq}{n}}
$$

When p = pF, we find $N \geq 6520$. Hence, the minimal number N that will satisfy the confidence interval are within $\pm$0, 0,1 is 6520, such that we need to satisfy the confidence interval are within ±0, 01 is 6520, such that we need to test 6520-1200 more people.

```python
from scipy.stats import t
import numpy as np

radi = np.array([5.2, 5.7, 7.6, 8.0, 7.7, 5.5, 6.7, 7.0, 8.4, 5.9])
belt = np.array([5.1, 5.9, 7.2, 7.9, 7.8, 5.4, 6.7, 6.8, 7.9, 5.7])
n = len(a)
 
x = belt-radi
xbar = x.mean()
S=sqrt(sum(np.power(x-xbar,2))/(n-1))
TS=sqrt(n)/S*xbar
pvalue=2*t.cdf(TS,n-1)
(TS, pvalue)
> (-1.9475670608117976, 0.08329148695657175)
```

At 5% confidence interval, the hypothesis is accepted, and the belted tires are better.

### C)
To ensure they drive the same way?

