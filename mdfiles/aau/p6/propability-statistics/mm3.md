# Expectation and variance

# Pre-Problems
Imagine that you are asked to play the following game: 
A die is rolled. If an odd number turns up, you win the amount equal to this number; if an even number turns up, you lose an amount equal to this 
number. 
Would you be wiling to play it?

Seeing as odd would mean [1,3,5], and the even would be [2,4,6], my chances are not well.

# Notes from slides
## Expectation

# Problems
The density function of X is given by

$$
f(x) = 
\begin{pmatrix}
a+bx^2 & 0 \leq x \leq 1 \\\\
0 & otherwise \\\\
\end{pmatrix}
$$


If E[X] = 3/5, find a and b.

From MM3s5-6 and from MM2, we have

$$
\int\_{\infty}^{\infty} |x|f(x) dx < \infty
\int_{-\infty}^{\infty} f(x)dx = 1
$$

Using these two equations, with the first being equal to 3/5, it is possible for us to find a and b.


$$
\begin{align}
\int\_{-\infty}^{\infty} f(x)dx = 1 \\\\
\int\_{0}^{1} a+bx^2 dx = 1 \\\\
\left[ ax+\frac{bx^3}{3} \right]\_{0}^{1} = 1 \\\\
a+\frac{b}{3} - 0 = 1 \\\\
\end{align}
$$

$$
\int\_{\infty}^{\infty} |x|f(x) dx < \infty \\\\
\int\_{0}^{1} ax+bx^3) dx = \frac{3}{5} \\\\
\left[ \frac{ax^2}{2}+\frac{bx^4}{4} \right]\_{0}^{1} = \frac{3}{5} \\\\
\frac{a}{2}+\frac{b}{4} = \frac{3}{5} \\\\
$$

Now we have two equations for two unknowns.


$$
a+\frac{1}{3}b  = 1 \\\\
a+\frac{1}{2}b = \frac{6}{5} \\\\
$$


$$
\begin{align}
a+\frac{1}{3}b  = 1 \\\\
a=1-\frac{1}{3}b \\\\
\text{insert} \\\\
1-\frac{1}{3}b+\frac{1}{2}b = \frac{6}{5} \\\\
1+\frac{1}{6}b = \frac{6}{5} \\\\
b = \frac{\frac{1}{5}}{\frac{1}{6}} \\\\
b = \frac{6}{5} = 1,2 \\\\
\text{insert} \\\\
a=1-(\frac{1}{3} \cdot \frac{6}{5})\\\\
a = 0,6 \\\\
\end{align}
$$

## Problem 3.2 (problem 4.32 from Sheldon Ross, 3rd ed.)
If E[X] = 2 and E[X2] = 8, calculate

### a) $E[(2 + 4X)^2]$

$$
\begin{align}
E[(2 + 4X)^2] =& \\\\
&= E[4+16X+16X^2] \\\\
&= E[4] + E[16X]+E[16X^2] \\\\
&= 4 + 16 \cdot 2 + 16 \cdot 8 = 164 \\\\
\end{align}
$$

### b) $E[X^2 + (X + 1)^2]$
$$
\begin{align}
E[X^2 + (X + 1)^2] =& \\\\
&= E[X^2 + X^2+1+2X] \\\\
&= E[2X^2+1+2X] \\\\
&= E[1+2X+2X^2] \\\\
&= 1+2 \cdot 2 + 2 \cdot 8 = 21 \\\\
\end{align}
$$

## Problem 3.3 (problem 4.45 from Sheldon Ross, 3rd ed.)
A product is classified according to the number of defects it contains and the factory that produces it.
Let $X\_1$ and $X\_2$ be the random variables that represent the number of defects per unit (taking the possible values of 0, 1, 2, or 3) and the factory number (taking on possible values 1 or 2), respectively.
The entries in the table represent the joint probability mass function of a random chosen product:

- P(0, 1) = 1/8; P(0, 2) = 1/16; P(1, 1) = 1/16; P(1, 2) = 1/16; P(2, 1) = 3/16; P(2, 2) = 1/8; P(3, 1) = 1/8; P(3, 2) = 1/4

Which we can put into a table, like we learned in MM2, joint dis TODO(insert link)

| Y/X |        |      | X    |      |     |
|-----|--------|------|------|------|-----|
|     | f(x,y) | 0    | 1    | 2    | 3   |
| Y   | 1      | 1/8  | 1/16 | 3/16 | 1/8 |
|     | 2      | 1/16 | 1/16 | 1/8  | 1/4 |

In the table above, the rows are normally shifted.

### a) Find the marginal probability distributions of $X\_1$ and $X\_2$

$$
\begin{align}
P(X_1 = 0) = \frac{1}{8} + \frac{1}{16} = \frac{3}{16} \\\\
P(X_1 = 1) = \frac{1}{16} + \frac{1}{16} = \frac{1}{8} \\\\
P(X_1 = 2) = \frac{3}{16} + \frac{1}{8} = \frac{5}{16} \\\\
P(X_1 = 3) = \frac{1}{8} + \frac{1}{4} = \frac{3}{8} \\\\
P(Y_1 = 0) = \frac{1}{8} + \frac{1}{16} + \frac{3}{16} + \frac{1}{8} = \frac{1}{2} \\\\
P(Y_1 = 1) = \frac{1}{16} + \frac{1}{16} + \frac{1}{8} + \frac{1}{4} = \frac{1}{2} \\\\
\end{align}
$$

### b) Find $E[X\_1]$, $E[X\_2]$, $Var[X\_1]$, $Var[X\_2]$, and $Cov(X\_1, X\_2)$

First we will find values for $E[X\_1]$, $Var[X\_1]$, which also include $E[X\_1^2]$, as that is needed to find the variance as described in MM3S16 $Var(X) = E[X^2] - E[X]^2$.
The reason for using [0, 1, 2, 3], is because $X\_1$ can take the values of that, as given in the assignment.

TODO(eyJhb) check if the sum below works

$$
\begin{align}
E[X\_1] = \sum\_i i \cdot P(X=i) = 0 \cdot \frac{3}{16} + 1 \cdot \frac{1}{8} +  2 \cdot \frac{5}{16} +  3 \cdot \frac{3}{8} = 1,875 \\\\
E[X\_1^2] = \sum\_i i^2 \cdot P(X=i) = 0^2 \cdot \frac{3}{16} + 1^2 \cdot \frac{1}{8} +  2^2 \cdot \frac{5}{16} +  3^2 \cdot \frac{3}{8} = 4,75 \\\\
Var(X\_1) = 4,75 - (1,875)^2 = 1.2343 \\\\
\end{align}
$$


Here we use [1, 2], as those are the values that $X\_2$ can take.

$$
\begin{align}
E[X\_2] = 1 \cdot \frac{1}{2} + 2 \cdot \frac{1}{2} = 1,5 \\\\
E[X\_2^2] = 1^2 \cdot \frac{1}{2} + 2^2 \cdot \frac{1}{2} = 2,5 \\\\
Var(X\_2) = 2,5 - (1,5)^2 = 0,25 \\\\
\end{align}
$$

Covariance is given on MM3S23, as $Cov(X,Y) = E[XY] - E[X]E[Y]$, and to calculate the $E[XY]$ we need the formula on MM3S13.
How this works is by taking all the indexes in the table, (x,y) and multiplying with the possibility, see the example below where the zero indexes have been left out (0,1;0,2, etc).

$$
\begin{align}
E[X\_1X\_2] &= 1 \cdot 1/16 + 2 \cdot 3/16 + 3 \cdot 1/8 + 2 \cdot 1/16 + 4 \cdot 1/8 + 6 \cdot 1/4 = \frac{47}{16} \\\\
E[X\_1]E[X\_2] &= (0 \cdot \frac{3}{16} + 1 \cdot \frac{1}{8} +  2 \cdot \frac{5}{16} +  3 \cdot \frac{3}{8}) \cdot (1 \cdot \frac{1}{2} + 2 \cdot \frac{1}{2}) = \frac{45}{16} \\\\
Cov(X\_1,X\_2) &= \frac{47}{16} - \frac{45}{16} = \frac{1}{8} \\\\
\end{align}
$$

## Problem 3.4 (problem 4.53 from Sheldon Ross, 3rd ed.)
Suppose that X has density function $f(x)=e^{-x}, x > 0$.
Compute the moment generating function of X and use your results to determine its means and variance. Check your answer fir the mean by a direct calculation.

On MM3S32 it is shown how this can be done, and done so below.
Keep in mind the criteria for the t value, that is set.

$$
\begin{align}
\varphi(t) = E[e^{tX}] &= \int\_{\infty}^{\infty} e^{tx}f(x) dx \\\\
&= \int\_{0}^{\infty} e^{tx}e^{-x} dx \\\\
&= \int\_{0}^{\infty} e^{(t-1)x} dx \\\\
&= \left[ \frac{1}{t-1}e^{(t-1)x} \right]\_0^{\infty} \\\\
&= \frac{1}{t-1}, for t < 1 \\\\
\end{align}
$$

From the slides it is known that we can obtain all moments of X, bf differentiating the function for t.
To find the mean value, we do this once and set $t=0$.
Source: https://study.com/academy/lesson/moment-generating-functions-definition-equations-examples.html

$$
\begin{align}
E[X] &= \left. \frac{d}{dt} E[e^{tx}] \right|\_{t=0} \\\\
&= \left. \frac{d}{dt} \frac{1}{t-1} \right|\_{t=0} \\\\
&= \left. \frac{1}{(t-1)^2} \right|\_{t=0} \\\\
&= \frac{1}{1} = 1 \\\\
\end{align}
$$

Now to find the var ($Var(X) = E[X^2] - E[X]^2$), we need to find $E[X^2]$, this can be done by differentiating it two times and setting $t=0$ again.

$$
\begin{align}
E[X^2] &= \left. \frac{d}{dt} E[X] \right|\_{t=0} \\\\
&= \left. \frac{d}{dt} \frac{1}{(t-1)^2} \right|\_{t=0} \\\\
&= \left. \frac{-2}{(t-1)^3} \right|\_{t=0} \\\\
&= \frac{-2}{-1} = 2 \\\\
\end{align}
$$

$$
\begin{align}
Var(X) &= E[X^2] - E[X]^2 \\\
&= 2 - 1^2 = 1 \\\
\end{align}
$$

We now need to do it by calculation, where we can use MM3S31 to do this.
For this we are doing integration by parts, which means $\int\_{a}^{b} u(x)v'(x) dx = \left[ u(x)v(x) \right]\_a^b - \int\_a^b u'(x)v(x) dx$

$$
\begin{align}
E[X^n] &= \int\_{-\infty}^{\infty} x^n f(x) dx \\\\
E[X] &= \int\_{0}^{\infty} x e^{-x} dx \\\\
&= \left[ -xe^{-x)} \right]\_0^{\infty} - \int\_{0}^{\infty} e^{-x} dx \\\\
&= - \int\_{0}^{\infty} e^{-x} dx \\\\
&= - \left[ -e^{-x} \right]\_{0}^{\infty} \\\\
&= - (-1) = 1 \\\\
\end{align}
$$

$$
\begin{align}
E[X^n] &= \int\_{-\infty}^{\infty} x^n f(x) dx \\\\
E[X^2] &= \int\_{0}^{\infty} x^2e^{-x} dx \\\\
&= \left[ -x^2e^{-x)} \right]\_0^{\infty} - 2\int\_{0}^{\infty} e^{-x} dx \\\\
&= - 2\int\_{0}^{\infty} e^{-x} dx \\\\
&= - 2\left[ -e^{-x} \right]\_{0}^{\infty} \\\\
&= - 2(-1) = 2 \\\\
\end{align}
$$

Var already calculated, and matches what was found here

## Problem 3.5
A cdf of a r.v. X is

$$
F(x) = 
\begin{pmatrix}
0 & x \leq 0 \\\\
x/4 & 0 < x \leq 4 \\\\
1 & x > 4\\\\
\end{pmatrix}
$$

Find E[X].

This is a two part function, as we both has discrete values and a continuous one, as can be seen in the graph below.

```asy
import graph; 
size(500); 
Label f; 

real f(real x) 
{ 
    if(x <= 0) {
        return 0;
    } else if((0 < x) && (x <= 4)) {
        return x/4;
    } else if(x > 4) {
        return 1;
    } 
    return 99999999999999; 
} 

draw(graph(f,-0.3,5),green+linewidth(1));
xaxis(BottomTop,Ticks(f, 0.2,extend=true)); 
yaxis(LeftRight,Ticks(f, 0.1,extend=true)); 
```

The continuous part is when $x/4, when 0 < x \leq 4$, whereas the two other parts are discrete.
How to calculate these can be found on MM3S5, where we first take all the discrete values and add together, followed by the continuous.
The probability for the discrete values are given as the jump, so from $x \leq 0$ to $0 < x$, there is a step of $0$, and from $x \leq 4$ to $x > 4$, there is a step of $0$, therefore we can leave out the discrete values.

$$
\begin{align}
E[X] &= \sum\_{k=0} P(X > k) + \int\_{0}^{\infty} (1-F(x)) dx \\\\
&= \int\_{0}^{4} (1-\frac{x}{4}) dx \\\\
&= \left[x-\frac{x^2}{8} \right]\_{0}^{4} \\\\
&= 4-\frac{4^2}{8} \\\\
&= 4-\frac{16}{8} \\\\
&= 4-2 = 2 \\\\
\end{align}
$$

This makes sense, as it can be seen that the found mean value is in the middle of the continuous line.

```asy
import graph; 
size(500); 
Label f; 

real f(real x) 
{ 
    if(x <= 0) {
        return 0;
    } else if((0 < x) && (x <= 4)) {
        return x/4;
    } else if(x > 4) {
        return 1;
    } 
    return 99999999999999; 
} 

draw(graph(f,-0.3,5),green+linewidth(1));
xaxis(BottomTop,Ticks(f, 0.2,extend=true)); 
yaxis(LeftRight,Ticks(f, 0.1,extend=true)); 
draw((2,0)--(2,0.5)--(0,0.5),dashed+red+linewidth(2));
```

Used this example for help: https://www.youtube.com/watch?v=ZCDxnLJC-HE , where the given values are (but he uses another one where he has to convert it to pdf, by differentiating first?):

$$
F(x) = 
\begin{pmatrix}
0 & x < 0 \\\\
\frac{x+1}{4} & 0 \leq x < 1 \\\\
1 & 1 \leq x \\\\
\end{pmatrix}
$$

With the following graph:

```asy
import graph; 
size(500); 
Label f; 

real f(real x) 
{ 
    if(x < 0) {
        return 0;
    } else if((0 <= x) && (x < 1)) {
        return (x+1)/4;
    } else if(1 <= x) {
        return 1;
    } 
    return 99999999999999; 
} 

draw(graph(f,-0.3,5),green+linewidth(1));
xaxis(BottomTop,Ticks(f, 0.2,extend=true)); 
yaxis(LeftRight,Ticks(f, 0.1,extend=true)); 
dot((5/8,((5/8)+1/4)),red+linewidth(5));
```

Where it would look like this:

$$
\begin{align}
\frac{d}{dx} \frac{x+1}{4} &= \frac{1}{4} \\\\
E[X] &= \sum\_i x\_i P(X=x\_i) + \int\_{-\infty}^{\infty} xf(x) dx \\\\
&= (0 \cdot \frac{1}{4}) + (1 \cdot \frac{1}{2}) + \int\_{0}^{1} x\frac{1}{4} dx \\\\
&= \frac{1}{2} + \int\_{0}^{1} \frac{x}{4} dx \\\\
&= \frac{1}{2} + \left[\frac{x^2}{8} \right]\_{0}^{1} \\\\
&= \frac{1}{2} + \frac{1^2}{8} \\\\
&= \frac{4}{8} + \frac{1}{8} \\\\
&= \frac{5}{8} \\\\
\end{align}
$$

# Unused
$$
\begin{align}
E[X] &= \sum\_i x\_i P(X=x\_i) + \int\_{-\infty}^{\infty} xf(x) dx \\\\
&= (0 \cdot 0) + (1 \cdot 0) + \int\_{0}^{4} x\frac{x}{4} dx \\\\
&= \int\_{0}^{4} \frac{x^2}{4} dx \\\\
&= \left[\frac{x^3}{12} \right]\_{0}^{4} \\\\
&= \frac{4^3}{12} \\\\
&= \frac{64}{12} \\\\
&= \frac{16}{3} \approx 5,3333 \\\\
\end{align}
$$

