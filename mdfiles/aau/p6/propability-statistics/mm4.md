# Special random variables (examples of distributions)

# Notes from slides
## Binomial Random Value
MM4S15, general formula + mean and variance

# Problems

## Problem 4.1 (problem 5.2 from Sheldon Ross, 3rd ed.) (direct solution taken)
A communication channel transmits the digits 0 and 1.
However, due to static, the digit transmitted is incorrectly received with probability 0.2.
Suppose that we want to transmit an important message consisting of one binary digit.
To reduce chances of error, we transmit 00000 instead of 0 and 11111 instead of 1.
If the receiver of the message uses ”majority” decoding, what is the probability that the message will be incorrectly decoded?
What independence assumptions are you making?

Using MM4S19, or MM4S15

Given P(a transmitted digit is received incorrectly) = 0.2.
Let X be a r.v. representing a number of incorrectly received digits out of 5.
X is a binomial r.v. with parameters (5, 0.2) X $\tilde b(n = 5, p = 0.2)$.
The message is decoded correctly if at least 3 out of 5 digits are received correctly, or in other words, the number of errors is less or equal to 2.

$$
\begin{align}
P(X \leq i) &= \sum\_{k=0}^i
\begin{pmatrix}
n \\\\
k
\end{pmatrix}
p^k(1-p)^{n-k} \\\\
\begin{pmatrix}
n \\\\
k
\end{pmatrix}
&= \frac{n!}{k!(n-k)!}
\end{align}
$$

$$
\begin{align}
P(X \leq 2) &= \sum\_{k=0}^i
\begin{pmatrix}
5 \\\\
k
\end{pmatrix}
0,2^{k}(1-0,2)^{5-k} \\\\
&= \begin{pmatrix}
5 \\\\
0
\end{pmatrix}
0,2^{0}(1-0,2)^{5-0}
+
\begin{pmatrix}
5 \\\\
1
\end{pmatrix}
0,2^{1}(1-0,2)^{5-1}
+
\begin{pmatrix}
5 \\\\
2
\end{pmatrix}
0,2^{2}(1-0,2)^{5-2} \\\\
&= 0,3276 + 0,4096 + 0,2048 = 0.9421
\end{align}
$$

```python
>>> p = 0.20
>>> k = change
>>> n = 5
>>> (factorial(n)/(factorial(k)*factorial(n-k)))*p**k*(1-p)**(n-k)
```

Thus, probability that the message is decoded incorrectly can be found as

$$
P(incorrectly) = 1 - P(X \leq 2) = 0.0579
$$

The above derivations are done under assumption that errors in transmitted digits are independent.

## Problem 4.2 (problem 5.4 from Sheldon Ross, 3rd ed.) (direct solution taken)
Suppose that a particular trait (such as eye colour or left-handedness) of a person is classified on the basis of one pair of genes, and suppose that *d* represents a dominant gene and *r* represents a recessive gene.
Thus, a person with *dd* genes is pure dominant, one with *rr* is pure recessive, and one with *rd* is hybrid.
The pure dominance and the hybrid are alike in appearance.
Children receive one gene from each parent.
If, with respect to a Particular trait, 2 hybrid parents have a total of 4 children, what is the probability that 3 of the 4 children have the outward appearance of the dominant gene?

It is given that the parents are of hybrid type, that is they have genes r1d1 and r2d2. Since children receive 1 gene from each parent, they can have the following genes:

- $r\_1r\_2$
- $r\_1d\_2$
- $d\_1r\_2$
- $d\_1d\_2$

All these 4 outcomes are equiprobable.
A child will have the appearance of the dominant gene in the last 3 cases ($r\_1d\_2$, $d\_1r\_2$ or $d\_1d\_2$).
Thus, probability that a child has the appearance of the dominant gene is

$$
p = \frac{3}{4}
$$

Let X be a r.v. representing a number of children that have the appearance of the dominant gene $X \tilde b(n = 4, p = 0.75)$.
Probability that X is equal to exactly 3 can be found as (using the same slide as from before):

$$
\begin{align}
P(X = 3) = \begin{pmatrix}
4 \\\\
3
\end{pmatrix}
0,75^3(1-0,75)^{4-3} = 0,4219 \\\\
\end{align}
$$

```python
>>> p = 0.75
>>> k = 3
>>> n = 4
>>> (factorial(n)/(factorial(k)*factorial(n-k)))*p**k*(1-p)**(n-k
```

## Problem 4.3 (problem 5.11 from Sheldon Ross, 3rd ed.)
If you buy a lottery ticket in 50 lotteries, in each of which your chance of winning a prize is 1/100, what is (approximate) probability that you will win a prize

We can use Poisson for this, as n is large and p is small, we could also use binomial for it, but this is easier, check slides MM4S27

Let r.v. X represents the number of times you win a prize.
If n is large and p is small, we can assume that X is approximately Poisson distributed.

$$
\begin{align}
n &= 50 \\\\
p &= \frac{1}{100} \\\\
\lambda &= 50\frac{1}{100} = 0,5
\end{align}
$$

### a) at least once
$$
\begin{align}
P(X = k) &= e^{-\lambda}\frac{\lambda^{k}}{k!} \\\\
P(X \geq 1) &= 1 - e^{-\lambda}\frac{\lambda^{0}}{0!} \\\\
&= 1 - e^{-\lambda} \\\\
&= 1 - e^{-\lambda} 0,3934 \\\\
\end{align}
$$


### b) exactly once
$$
\begin{align}
P(X = 1) &= e^{-\lambda}\frac{\lambda^{1}}{1!} = 0,3033 \\\\
\end{align}
$$

### c) at least twice?
$$
\begin{align}
P(X \geq 2) &= 1 - P(X=2) \\\\
&= 1 - P(X = 0) - P(X = 1) \\\\
&= 1 - e^{-\lambda} - 0,3033 = 0,0902
\end{align}
$$

## Problem 4.4 (problem 5.17 from Sheldon Ross, 3rd ed.)
If X is a Poisson r.v. with mean $\lambda$, show that $P(X = i)$ first increases and then decreases as i increases, reaching its maximum value when i is the largest integer less or equal to $\lambda$.
