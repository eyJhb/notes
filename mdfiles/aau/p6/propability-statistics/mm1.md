# Elements of Probability
The concept of the probability of a particular event of an experiment will be introduced by using probability axioms. Subsequently, the following topics will be covered:  Sample Space. Events. Set Operations: union, intersection, compliment. Computing probabilities with counting methods. Conditional probability. Bayes’ formula. Independent events.

# Before Lecture Exercise
Lets assume that we have a fair coin, and it is equally likely to have head or tail as an outcome of an experiment.
We repeatedly toss a coin and write down the outcome (we write ”1” for a head and ”0” for a tail).
 Before we toss a coin, we do not know what we will see as a result.
However, we would like to determine the probability that we get a head.
We can use a relative frequency interpretation and estimate it as

$$ 
P(head) = \frac{\text{no of times head is observed}}{\text{total no of tosses}}
$$

- a) calculate probability when you repeat the procedure 10 times; 100 times; 1000 times;
- b) repeat the same experiment several times and compare the results.

The exercise asks to do this in MATLAB, but can easily be solved using the following program

```python
import random
import sys

# used to index results
TAIL = 0
HEAD = 1

results = [0, 0] # index 0 for tail, 1 for head

# get arguments
if len(sys.argv) < 2:
    print("CALL WITH ARGUMENT NUMBER OF ROLLS")
    exit(1)

rollsArgs = sys.argv[1]
try:
    rolls = int(rollsArgs)
except:
    print("The provided argument is not a int: "+str(rollsArgs))
    exit(1)

for i in range(0, rolls):
    results[random.randint(0,1)] += 1

print("Number of tails: "+str(results[TAIL]))
print("Number of heads: "+str(results[HEAD]))
print("P(HEAD) = "+str(results[HEAD]/(results[HEAD]+results[TAIL])))
```

Output of this is the following:

```
[eyjhb@eos:~/projects/git/notes]$ python test.py 10
Number of tails: 5
Number of heads: 3
P(HEAD) = 0.5

[eyjhb@eos:~/projects/git/notes]$ python test.py 100
Number of tails: 49
Number of heads: 51
P(HEAD) = 0.51

[eyjhb@eos:~/projects/git/notes]$ python test.py 1000
Number of tails: 485
Number of heads: 515
P(HEAD) = 0.515
```

# Slide notes
## Deterministic models vs probabilistic models
- Deterministic model: the conditions under which an experiment is carried out determine the exact outcome of the experiment
- Probabilistic model: the outcome varies in an unpredictable fashion when the experiment is repeated under the same conditions

## Set operations
- *Union* of A and B $A \cup B$ = {all outcomes that are either in A or B}
- *Intersection* of A and B; $AB = A \cap B$ {all outcomes that are both in A and B}
- Two events are mutually *exclusive*, if $AB=\emptyset$
- The *complement* of a event A; $A^c=\overline{A}$ {all events that are not in A}
- If all outcomes of B are in A, B is *contained* in A; $B\subset A$
- The definitions can be generalised for the case of n events
- Graphical representation of events can be made by *Venn diagrams*, see [example](#mm1-venn-example)

To expand of this, we have the following laws

- **Cummutative law** $E \cup F = F \cup E \qquad EF=FE$
- **Associative law** $(E \cup F) \cup G = E \cup (F \cup G) \qquad (EF)G=E(FG)$
- **Distributive law** $(E \cup F)G = EG \cup FG \qquad EF \cup G = (E \cup G)(F \cup G)$

## DeMorgan's Law

$$
\begin{align}
(E \cup F)^c = E^c F^c \\\\
(EF)^c = E^c \cup F^c \\\\
\end{align}
$$

## Axioms of probability
Let *E* be a random experiment.
A probability law for the experiment *E* is a rule that assigns to each event *A* a number *p(A)*, called the *probability of A*, that satisfies the following axioms: 

### Axiom 1
$$0 \leq P(A) \leq 1$$

### Axiom 2
$$P(S) = 1$$

### Axiom 3
For any sequence of mutually exclusive events

$$
P(\cup\_{i=1}^n A\_i )=\Sigma\_{i=1}^n P(A\_i)
$$

## Propositions

### Proposition 1 {#proposition-1}
$$
P(A^c)=1-P(A)
$$

### Proposition 3 {#proposition-2}
$$
P(A \cup B) = P(A) + P(B) - P(AB)
$$

### Proposition 3 {#proposition-3}
If $A \subset B$, then $P(A) \leq P(B)$


## Venn Diagram Examples {#mm1-venn-example}

![venn diagram](figures/venn-diagram.png)

## Bayes' Formula {#bayes-formula}
A and B are two events $A = AB \cup AB^c$, then

$$
P(A) = P(AB) + P(AB^c) = P(A|B)P(B) + P(A|B^c)P(B^c)
$$

The probability of event A is a weighted average of conditional probabilities.
Suppose that A has occurred and we are interested in determining if B has also occurred:

$$
P(A|B) = \frac{P(B|A)P(A)}{P(B)}
$$

NOTE! The above is from Wikipedia [here](https://en.wikipedia.org/wiki/Bayes%27_theorem), while the below is from MM1 slides.

$$
P(B|A) = \frac{P(B|A)}{P(A)} = \frac{P(A|B)P(B)}{P(A|B)P(B) + P(A|B^c)P(B^c)}
$$

## Independence of events {#indepence-of-events}
Generally, knowing that B has occurred, changes the chances of A's occurrence: $P(AB)=P(A)P(B|A)$ (dependent).
If it does not, then $P(A|B)=P(A)$ (for P(B) is not zero, and invert for P(A) not zero) and are therefore independent.

- Definition: Two events are independent if:
    - $P(AB)=P(A)P(B)$.
- Definition: Three events are independent if:
    - $P(ABC)=P(A)P(B)P(C)$
    - $P(AB)=P(A)P(B)$
    - $P(BC)=P(B)P(C)$
    - $P(AC)=P(A)P(C)$
- Definition: N events are independent, if for any subset:
    - $P(A_{r1} \dots A_{rk}) = P(A_{r1}) \dots P(A_{rk})

## Sampling with/without Replacement and with/without ordering
First the terms with/without replacement and with/without ordering explained

- Ordered versus unordered samples: In ordered samples, the order of the elements in the sample matters; e.g., digits in a phone number, or the letters in a word. In unordered samples the order of the elements is irrelevant; e.g., elements in a subset, or lottery numbers.[^2]
- Samples with replacement versus samples without replacement: In the first case, repetition of the same element is allowed (e.g., numbers in a license plate); in the second, repetition not allowed (as in a lottery drawing—once a number has been drawn,it cannot be drawn again).[^2]

For example, if we have *N* objects in a basket and we choose *k* objects from that basket.
What is the number of possible outcomes?

### Sampling *with replacement* and *with ordering*
$$n^k$$

### Sampling *without replacement* and *with ordering*
$$n(n-1) \dots (n-k+1)$$

### Sampling *without replacement* and *without ordering* {#samp-worepl-woord}
$$
\frac{n(n-1) \dots (n-k+1)}{k!} = \frac{n!}{(n-k)!k!} = 
\begin{pmatrix}
n \\\\
k
\end{pmatrix}
$$

### Sampling *with replacement* and *without ordering*
$$
\begin{pmatrix}
n - 1 + k\\\\
k
\end{pmatrix}
$$

## Conditional probability
We are often interested in calculating probabilities when some partial information concerning the results of the experiment is available; or recalculating it in light of new information.
It is often turns out that it is easier to compute the probability of an event if we first ”condition” on the occurence or non-occurence of a secondary event.
The conditional probability is defined as:

$$
P(A|B) = \frac{P(AB)}{P(B)}, P(B) > 0
$$

# Notes
## Backslash notation
The backslash notation indicates "relative complement" or "set difference", which means the set $A \\ B$ consists of all those elements that are in A but not in B.
A example of this could be the probability $P(A \\ B)$, which means "the probability that event A occurs but event B does not occurs".

More information can be found here [^1].

# Exercises
## Problem 1.1 (problem 3.3 from Sheldon, 3rd ed.)
Let $S = \\{1,2,3,4,6,7\\}$; $E=\\{1,3,5,7\\}$; $F=\\{7,4,6\\}$; $G=\\{1,4\\}$. Find

- a) $EF$

$$ EF = E \cap F = 7 $$

- b) $E \cup FG$

$$
E \cup FG = E \cup (F \cap G)
= \\{1,3,5,7\\} \cup \\{4\\}
= \\{1,3,4,5,7\\}
$$

- c) $EG^c$

When we have $G^c$, then it means everything not in $G$, which in our case with our set $S=1..7$, would be $G^c=\\{2,3,5,6,7\\}$.

$$ EG^c = \\{1,3,5,7\\} \cap \\{2,3,5,6,7\\} = \\{3,5,7\\} $$

- d) $EF^c \cup G$

Firstly lets find $F^c = \\{1,2,3,5\\}$.

$$
(EF^c) \cup G
= (\\{1,3,5,7\\} \cap \\{1,2,3,5\\}) \cup \\{1,4\\}
= \\{1,3,5\\} \cup \\{1,4\\}
= \\{1,3,4,5\\}
$$

- e) $E^c(F \cup G)$

First lets find $E^c=\\{2,4,6\\}$.

$$
\\{2,4,6\\} \cap (\\{7,4,6\\} \cup \\{1,4\\})
= \\{2,4,6\\} \cap \\{1,4,6,7\\}
= \\{4,6\\}
$$

- f) $EG \cup FG$

$$
(E \cap G) \cup (F \cap G)
= (\\{1,3,5,7\\} \cap \\{1,4\\}) \cup (\\{7,4,6\\} \cap \\{1,4\\})
= \\{1\\} \cup \\{4\\}
= \\{1,4\\}
$$

## Problem 1.2
10 books are randomly placed on a shelf.

- a) How many distinct sequences of books are possible?

When we have 10 books that are randomly place on the shelf, then first there is 10 ways, the second in 9 ways, etc.
Mathematically this can be written as:

$$10 \cdot 9 \cdot 8 \dots 1 = 10!$$

- b) How many distinct sequences of books are possible, if we know that two particular books of those ten books are placed next to each other?

If we know that two books are always placed next to each other, then we can consider that we have one less sequence we can take.
This means that first there are 9 ways, then 8, etc. all the way down to 1, so we again have $9!$.
But we should also keep in mind, these two books can be placed in two different ways, $AB$, $BA$, which gives us another possibility of $2$, thus the answer is.

$$
9! \cdot 2
$$

## Problem 1.3
Show that the probability that exactly one of the events E or F occurs is equal to $P(E)+P(F)-2P(EF)$.

If we have the probability that either events E or F occurs, then it also implies that we do not want when E and F occurs, but only when either of them does.
This gives us the following notation:

$$
P((E \cup F) \\ (E \cap F))
$$

This can be further expanded on using our [proposition 2](#proposition-2).

$$
\begin{align}
P((E \cup F) \\ (E \cap F)) 
& = P((E \cup F) \\ (EF)) \\\\
& = (P(E) + P(F) - P(EF)) - P(EF) \\\\
& = P(E) + P(F) - 2P(EF) \\\\
\end{align}
$$

Which proof what we were asked.

## Problem 1.4
A transmitter can send two messages A and B.
Message A is sent with probability 0.84 and message B is sent with probability 0.16.
Due to transmission errors, 1/6 of all sent messages of type A are received as message of type B.
1/8 of all sent messages of type B are received as message of type A.

The following denotation will be used, $A_r$ message A received, $A_t$ message A transmitted same goes for B.
So e.g. $P(A_r|B_t)$ means the probability that message A was received, if message B was transmitted.

- a) Find the probability that message A is received

$$
\begin{align}
P(A_r) 
&= P(A_r|A_t) \cdot P(A_t) + P(A_r|B_t) \cdot P(B_t) \\\\
&= \frac{5}{6} \cdot 0.84 + \frac{1}{8} \cdot 0.16  \\\\
&= 0.72 \\\\
\end{align}
$$

The reason for this is, there is a 1/6 chance of the message A being sent as B, which leaves 5/6 of it being transmitted correctly.
And there is a chance of 1/8 that message B will be received as message A, which gives us this 1/8 chance.

- b) Find the probability that message B is received

Using the same principles as above, but just for $B_r$ instead.

$$
\begin{align}
P(B_r) 
&= P(B_r|B_t) \cdot P(B_t) + P(B_r|A_t) \cdot P(A_t) \\\\
&= \frac{7}{8} \cdot 0.16 + \frac{1}{6} \cdot 0.84  \\\\
&= 0.28 \\\\
\end{align}
$$

- c) Message A is received. Find the probability that message A was sent

We can solve this using [Bayes' Formula](#bayes-formula).

$$
\begin{align}
P(A_t|A_r)
&= \frac{P(A_r|A_t)P(A_t)}{P(A_r)} \\\\
&= \frac{\frac{5}{6} \cdot 0.84}{0.72} \\\\
&= 0.972
\end{align}
$$

## Problem 1.5 (problem 3.15 from Sheldon, 3rd ed.)
Show that 

$$
\begin{pmatrix}
n \\\\
r
\end{pmatrix}
= \begin{pmatrix}
n \\\\
n-r
\end{pmatrix}
$$

This can be shown by using our formula from sampling, where it is shown it is without replacement and without ordering [here](#samp-worepl-woord).

$$
\frac{n(n-1) \dots (n-r+1)}{r!} = \frac{n!}{(n-r)!r!} = 
\begin{pmatrix}
n \\\\
r
\end{pmatrix}
$$


Now we insert $r=n-r$, as we need to show it.

$$
\begin{align}
\frac{n!}{(n-r)!r!}
& = \frac{n!}{(n-(n-r))!(n-r)!} \\\\
& = \begin{pmatrix}
n \\\\
n-r
\end{pmatrix}
\end{align}
$$

Now present a combinatorial argument for the foregoing by explaining why a choice of r items from a set of n is equivalent to a choice of $n - r$ items from the set.

If you chose r objects among n objects, then there will always be $n-r$ objects left.
A example of this, if there is a basket with 10 objects, you pick $r=3$, then there are 7 left and you have taken 3.
If you instead have $n-r$, then you end up in the same scenario, as you remove 3 objects, and you again have 7 and 10 left.
It is a weird scenario to explain, and is not very well written.

## Problem 1.6
A basket contains 40 transistors: 5 of them are defective (= immediately fail when put in use); 10 are partially defective (fail after a couple of hours of use) and 25 are well functioning transistors.
A transistor is chosen at random and put in use.
If it does not fail immediately, what is the probability that it is acceptable?

Immediately we can remove all the defective transistors, as they are not part of the problem.
Then there are only 25 working, and $25+10$ in total, therefore we get the following probability. 

$$
P(R\_{working}) = \frac{25}{25+10} = \frac{5}{7} = 0.714
$$

# Post problems
## Problem 1
Determine whether the following experiments have a discrete (whole numbers, points etc.) or continuous (scales, time, measurement, things that could be infinity exact) outcomes: 

1. Throw a dart with a point tip at a dartboard
2. Choose a lottery number
3. Observe the outdoor temperature using an analog thermometer
4. Determine the current time in hours, minutes and seconds. 

1 and 2 are discrete, as we are dealing with whole exact numbers, whereas 3 and 4 are a measurement and are therefore continuous.

## Problem 2
Are independent events different from mutually exclusive events? 
Explain using a conditional probability. 

A independent means that that outcome of another event, does not affect this event.
Mutually exclusive events cannot happen at the same time. For example: when tossing a coin, the result can either be heads or tails but cannot be both[^3].
This means that mutually exclusive evens are dependent on each other, as the coin toss example stated (they cannot be the same, therefore depend on each other).

For independent events we have $P(AB)=P(A)P(B)$, and for mutually exclusive events $P(AB) = 0$.
Therefore they are different, because a mutually exclusive event is dependent on the outcome of the other.

This can be shown, by using the conditional probability, and using the mutually exclusion.

$$
P(A|B) = \frac{P(AB)}{P(B)}
= \frac{0}{P(B)}
= 0
$$

Here we inserted what is known about mutual exclusion, and get 0.
But we know that our $P(A|B)=P(A)$ cannot be 0, and therefore they are different.

## Problem 3
If $B \subset A$, what is the conditional probability of A given that B has occurred? 

If B is a subset of A, then we know that if B occurs, then A does as well because A contains B, this means that we expect $P(A)=1$.
We also know that $A \cap B=B$, then by using our conditional formula we get the following.

$$
P(A|B) = \frac{P(AB)}{P(B)}
= \frac{P(B)}{P(B)}
= 1
$$

Source: https://www.probabilitycourse.com/chapter1/1_4_0_conditional_probability.php

[^1]: Math Stackexchange about backslash syntax [here](https://math.stackexchange.com/questions/2921724/what-does-the-backslash-denote-in-probability-theory).
[^2]: Taken from pdf found [here](https://faculty.math.illinois.edu/~hildebr/408/408combinatorial.pdf)
[^3]: Math Stackexchange [here](https://math.stackexchange.com/a/941158)
