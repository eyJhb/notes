# Random variables
The concept of a random variables will be introduced.
Two types of random variables are considered: discrete and continuous.
We will learn how random variables can be specified using pmf/pdf.
The following topics are covered during the lecture: Discrete and continuous random variables.
Cumulative distribution function.
Probability mass function and probability density function.
Jointly distributed random variables.
Independent random variables.

# Pre-Problems
A red die and a blue die are tossed. The die that yields the larger number of dots is chosen and this number is written down. If both display the same number of dots, the red die is chosen. 
The outcomes of this experiment can be represented in a vector form, namely, by pairs (i, j) where i is either "blue" or "red" and j is the number of dots that is written down. 
Find probability of (red, 3).

First lets visualise it in a table, where the horizontal is blue, and the vertical axis is red.

| red/blue | 1  | 2  | 3  | 4  | 5  | 6  | blue |
|----------|----|----|----|----|----|----|------|
| 1        | >= | <  | <  | <  | <  | <  |      |
| 2        | >  | >= | <  | <  | <  | <  |      |
| 3        | >  | >  | >= | <  | <  | <  |      |
| 4        | >  | >  | >  | >= | <  | <  |      |
| 5        | >  | >  | >  | >  | >= | <  |      |
| 6        | >  | >  | >  | >  | >  | >= |      |
| red      |    |    |    |    |    |    |      |

From here we can see, that there is a total number of $6^2=36$ fields, where red has a $\frac{21}{36}$ chance to win, and blue has a $\frac{15}{36}$ chance to win.

There is a $\frac{1}{36}$ chance for the red die to be 3, whereas the blue die can get anything from 3 and under, therefore there is a $\frac{3}{36}$ for the blue to be 1, 2 or 3.
This can also be seen on the table above, where if the red die is 3, then the blue die can be 3, 2 or 1 and the red will still win.
This gives us a additional chance, and the final result will be:

$$
\frac{1}{36} + \frac{3}{36} = \frac{4}{36}
$$

This can be generalised to a N sided die, where the F (face) needs to be 3 as in this example.

$$
\frac{1+F}{N^2}
$$

# Slide Notes
## Types of random values

| Discrete Random Value                               | Continuous Random Value                      | Mixed Type |
|-----------------------------------------------------|----------------------------------------------|------------|
| PMF p(x)                                            | PDF f(x)                                     |            |
| $P\\{a < X < B \\} = \Sigma_{x_i \in (a,b)} p(x_i)$ | $P\\{a \leq X \leq B \\} = \int_a^b f(x) dx$ | $F(x)=p_1F_1(x) + \dots p_nF_n(x)$           |

## Cumulative distribution function {#cdf}
The CDF (cumulative distribution function) of a real-valued random variable X is the function given by:

$$
F_X(x) = P(X \leq x)
$$

Where the right-hand side represents the probability that the random variable X takes on a value less than or equal to x.
The probability that X lies in the semi-closed interval (a,b], where $a < b$, is therefore:

$$
P(a < X \leq b) = F_X(b) - F_X(a)
$$

Definition of two random values

$$
F_{XY}(x,y) = P(X \leq x, Y \leq y)
$$

And for any N random values:

$$
F_{X_1 \dots X_N}(x_1, \dots, x_N) = P(X_1 \leq x_1, \dots, X_N \leq X_N)
$$

### Properties {#cdf-props}
1. $0 \leq F(x) \leq 1$
2. $\lim\_{x \rightarrow \infty} F(x) = 1$
2. $\lim\_{x \rightarrow -\infty} F(x) = 0$
4. $F(x)$ is a non-decreasing function:
    - $F(a) \leq F(b)$ if $a < b$
5. $F(x)$ is continuous from the right:
    - $F(b) = \lim\limits_{h \rightarrow 0} F(b+h) = F(b+)$
6. Probability that a random value X takes on a specific value b is equal to the jump (step) of cdf at that point b:
    - $P(X = b) = F(b+)-F(b-)$

TODO(insert images?)


### Derived functions {#cdf-derived}
Sometimes, it is useful to study the opposite question and ask how often the random variable is above a particular level.
This is called the complementary cumulative distribution function (ccdf) or simply the tail distribution or exceedance, and is defined as

$$
\bar{F}_X(x) = P(X > x)= 1 - F_X(x)
$$

## Types of random values 
### Probability density function (PDF) {#pdf}
X is a continuous r.v. if there exists a non-negative function f(x), defined for all x having the property that for any set B of real numbers.

$$
P(X \in B) = \int_B f(x)dx
$$

Function f(x)is called a *probability density function* of X, and it should satisfy the below.

$$
\int_{-\infty}^{\infty} f(x)dx = 1
$$

Different propability statements can be expressed using PDF, if we let a=b, then.

$$
P(a \leq X \leq b) = \int_a^b f(x)dx
$$

$$
P(a = 0) = \int_a^a f(x)dx = 0
$$

## Jointly distributed continuous random values {#joint-rvs}
TODO(rest of joint, we need the table stuff for problem 3.3)
- We say that X and Y are jointly continous, if there exist a function f(x,y) defined for all real x and y, having the property that for every set C in the 2- dimensional plane
    - $P\\{(X,Y) \in C \\} = \int\int\_{(x,y) \in C} f(x,y) dxdy$
- f(x,y) is called joint probability density function 
    - $P\\{X \in [a,b], Y \in [c,d]\\} = \int\_c^d\int\_a^b f(x,y) dxdy$
- The marginal pdfs are obtained by integrating out the variables that are not of interest. 
    - $f\_X = \int\_{-\infty}^{\infty} f(x,y) dy$
    - $f\_Y = \int\_{-\infty}^{\infty} f(x,y) dx$
- The joint cumulative distribution function is defined as
    - $F(x,y)=P(X \leq x, Y \leq y)$

## Independent random values {#ind-rv}
- X and Y are independent, if for any two sets of real numbers A and B
    - $P(X \in A, Y \in B) = P(X \in A)P(Y \in B)$
- In terms of joint cumulative distribution function:
    - $F(a,b)=F\_X(a)F\_Y(b)$
- In terms of pmf (discrete r.v.) and pdf (continuous r.v.) 
    - $p(x,y)=(p\_X(x)p\_Y(y))$
    - $f(x,y)=(f\_X(x)f\_Y(y))$

# Own notes
## Geometric distribution {#geo-dis}
In probability theory and statistics, the geometric distribution is either of two discrete probability distributions:
    - The probability distribution of the number X of Bernoulli trials needed to get one success, supported on the set $\\{ 1, 2, 3, ... \\}$
    - The probability distribution of the number $Y = X - 1$ of failures before the first success, supported on the set $\\{ 0, 1, 2, 3, ... \\}$

The geometric distribution gives the probability that the first occurrence of success requires k independent trials, each with success probability p.
If the probability of success on each trial is p, then the probability that the kth trial (out of k trials) is the first success is $\Pr(X=k)=(1-p)^{k-1}p$, for k = 0, 1, 2, 3, ....

The above form of the geometric distribution is used for modeling the number of trials up to and including the first success.
By contrast, the following form of the geometric distribution is used for modeling the number of failures until the first success: $\Pr(Y=k)=(1-p)^{k}p$, for k = 0, 1, 2, 3, ....

In either case, the sequence of probabilities is a geometric sequence. For example, suppose an ordinary die is thrown repeatedly until the first time a "1" appears.
The probability distribution of the number of times it is thrown is supported on the infinite set { 1, 2, 3, ... } and is a geometric distribution with p = 1/6.
The geometric distribution is denoted by Geo(p) where $0 < p \leq 1$.

Source: https://en.wikipedia.org/wiki/Geometric_distribution

# Problems
## Problem 2.1 (problem 4.1 from Sheldon Ross, 3rd ed.)
Five men and five women are ranked according to their scores on an examination.
Assume that no two scores are alike and all 10! possible ranking are equally likely.
Let X denote the highest ranking achieved by a woman (e.g. X = 2 if the top-ranked person was male and the next-ranked person was female).
Find pmf of X, that is find $P(X = i), i = 1, 2, \dots , 10$.

First lets assert what the possibility that $X=1$, which indicates a female in the first place.
This can be looked as in the 1st place there can be chosen between 5 different women, and the rest can have a arbitrary order.
This gives us the following calculation:

$$
P(X = 1) = \frac{5 \cdot 9!}{10!} = 0,5
$$

Just because it might look confusing, keep in mind if there was no limit on the females had to be in the 1st place, there would be $10 \cdot 9! = 10!$, which would yield a possibility of 100%.
It can also be written as: $10 \cdot 9 \cdot 8 \cdot 7 \dots \cdot 1$, as we remove a person from the pool each time.
Next when $X = 1$, the one in 1st place is a male, and again there are 5 possibilities of men, followed by 5 more of women, where after the next 8 can be chosen at will.

$$
P(X = 2) = \frac{5 \cdot 5 \cdot 8!}{10!} = 0,2778
$$

This continues, where the number of men decreases, but the women can still be 5 each time.

$$
\begin{align}
P(X = 3) = \frac{5 \cdot 4 \cdot 5 \cdot 7!}{10!} &= 0,1389 \\\\
P(X = 4) = \frac{5 \cdot 4 \cdot 3 \cdot 5 \cdot 6!}{10!} &= 0,0595 \\\\
P(X = 5) = \frac{5 \cdot 4 \cdot 3 \cdot 2 \cdot 5 \cdot 5!}{10!} &= 0,0198 \\\\
P(X = 6) = \frac{5 \cdot 4 \cdot 3 \cdot 2 \cdot 1 \cdot 5 \cdot 4!}{10!} &= 0,004 \\\\
\end{align}
$$

As there are only 5 men, everything above $X > 6$ equal 0.

## Problem 2.2 (problem 4.4 from Sheldon Ross, 3rd ed.)
The distribution function of the random variable X is given

$$
F(x) = 
\begin{pmatrix}
0 & x < 0 \\\\
x/2 & 0 \leq x \leq 1 \\\\
2/3 & 1 \leq x \leq 2 \\\\
11/12 & 2 \leq x \leq 3 \\\\
1 & 3 \leq x 
\end{pmatrix}
$$

### a) Plot this distribution function.
```asy
import graph; 
size(500); 
Label f; 

bool betweenInc(real x, real min, real max) {
    if(min <= x && x <= max) {
        return true;
    }

    return false;
}

real f(real x) 
{ 
    if(x < 0) {
        return 0;
    } else if(betweenInc(x, 0, 1)) {
        return x/2;
    } else if(betweenInc(x, 1, 2)) {
        return 2/3;
    } else if(betweenInc(x, 2, 3)) {
        return 11/12;
    } else if(3 <= x) {
        return 1;
    }
    return 99999999999999; 
} 

draw(graph(f,-0.1,5),green+linewidth(1));
xaxis(BottomTop,Ticks(f, 0.2,extend=true)); 
yaxis(LeftRight,Ticks(f, 0.1,extend=true)); 
```

### b) what is $P(X > 1/2)$?
This can de done using the [derived CDF](#cdf-derived) (also called CCDF), where we ask how often it is below that value (invert it).
The reason for this, is that we want to find what is above $1/2$, where it is easier to find the probability below that.

$$
P(X > 1/2) = 1 - P(X \leq 1/2) = 1 - F(1/2) = 1 - \frac{1}{4} = \frac{3}{4}
$$

### c) what is $P(2 < X \leq 4)$?
We again use the knowledge form [CDF](#cdf), and can use the formula:

$$
P(2 < X \leq 4) = F(4) - F(2) = 1 - \frac{11}{12} = \frac{1}{12}
$$

### d) what is $P(X < 3)$?
From this we get

$$
P(X < 3) = F(3) = 11/12
$$

This can be read from the above table.

### e) what is $P(X = 1)$?
From our [CDF Properties](#cdf-props), we get: $P(X = b) = F(b+)-F(b-)$.
This gives us:

$$
P(X = 1) =\frac{2}{3}-\frac{1}{2}
= \frac{1}{6}
$$

## Problem 2.3 (problem 4.6 from Sheldon Ross, 3rd ed.)
The amount of time, in hours, that a computer functions before breaking down is a continuous random variable with probability density function given by.

$$
f(x) = 
\begin{pmatrix}
\lambda e^{\frac{-x}{100}} & x \geq 0 \\\\
0 & 0 < x \\\\
\end{pmatrix}
$$

First we can determine the value of $\lambda$, this can be done using what we know about the [probability density function](#pdf).
Keep in mind the limits change, because it is specified that anything under 0, is 0 and is therefore not defined/important.

$$
\begin{align}
\int\_{-\infty}^{\infty} f(x)dx = 1 \\\\
\int\_{0}^{\infty} \lambda e^{\frac{-x}{100}} dx = 1 \\\\
\lambda \int_{0}^{\infty} e^{\frac{-x}{100}} dx = 1 \\\\
\lambda \left[ -100e^{\frac{-x}{100}} \right]_0^{\infty} = 1 \\\\
\lambda (-100e^{\frac{-\infty}{100}} - (-100e^{\frac{-0}{100}})) = 1 \\\\
\lambda (0 - (-100)) = 1 \\\\
100\lambda = 1 \\\\
\lambda = \frac{1}{100} \\\\
\end{align}
$$

### a) What is the probability that a computer will function between 50 and 150 hours before breaking down?
This can be written down as a constraint $P(50 \leq X \leq 150)$, which gives us our new formula from PDF.

$$
\begin{align}
P(a \leq X \leq b) &= \int_a^b f(x)dx \\\\
P(50 \leq X \leq 150) &= \int\_{50}^{150} \frac{1}{100}e^{-\frac{x}{100}} dx \\\\
&= \frac{1}{100} \int\_{50}^{150} e^{-\frac{x}{100}} dx \\\\
&= \frac{1}{100} \left[ -100e^{-\frac{x}{100}} \right]\_{50}^{150} \\\\
&= \frac{-100}{100} (e^{-\frac{150}{100}} - e^{-\frac{50}{100}}) \\\\
&= - (e^{-\frac{150}{100}} - e^{-\frac{50}{100}}) \\\\
&= - (e^{-1,5} - e^{-0,5}) \\\\
&\approx 0,3834 \\\\
\end{align}
$$

### b) What is the probability that it will function less than 100 hours?
Keep in mind that the limits change, because we want the probability from 0 to 100 (less than 100 hours).

$$
\begin{align}
P(a \leq X \leq b) &= \int_a^b f(x)dx \\\\
P(0 \leq X \leq 100) &= \int\_{0}^{100} \frac{1}{100}e^{-\frac{x}{100}} dx \\\\
&= - (e^{-\frac{100}{100}} - e^{-\frac{0}{100}}) \\\\
&= - (e^{-1} - e^{0}) \\\\
&\approx 0,6321 \\\\
\end{align}
$$

## Problem 2.4 (problem 4.8 from Sheldon Ross, 3rd ed.)
If the density function of X equals:

$$
f(x) = 
\begin{pmatrix}
ce^{-2x} & x \geq 0 \\\\
0 & 0 < x \\\\
\end{pmatrix}
$$


### Find c. What is P(X > 2)?
First we calculate c, by using what we know from [cdf](#cdf).

$$
\begin{align}
\int\_{-\infty}^{\infty} f(x)dx = 1 \\\\
\int\_{0}^{\infty} ce^{-2x} dx = 1 \\\\
c \int\_{0}^{\infty} e^{-2x} dx = 1 \\\\
c \left[-\frac{1}{2} e^{-2x} \right]\_{0}^{\infty} = 1 \\\\
-\frac{1}{2}c \left[e^{-2x} \right]\_{0}^{\infty} = 1 \\\\
-\frac{1}{2}c (e^{-2\infty} - e^{-2 \cdot 0}) = 1 \\\\
-\frac{1}{2}c (0 - 1) = 1 \\\\
\frac{1}{2}c= 1 \\\\
c = 2 \\\\
\end{align}
$$

Now we can calculate $P(X > 2)$.

$$
\begin{align}
P(a \leq X \leq b) &= \int_a^b f(x)dx \\\\
P(2 \leq X \leq \infty) &= \int\_{2}^{\infty} 2e^{-2x} dx \\\\
&= 2 \int\_{2}^{\infty} e^{-2x} dx \\\\
&= 2 \left[-\frac{1}{2} e^{-2x} \right]\_{2}^{\infty} \\\\
&= -\frac{1}{2}2 (e^{-2 \cdot \infty} - e^{-2 \cdot 2}) \\\\
&= -1 (0 - e^{-4}) \\\\
&\approx 0,0183 \\\\
\end{align}
$$

## Problem 2.5 (problem 4.12 from Sheldon, 3rd ed.)
The joint density of X and Y is

$$
f(x) = 
\begin{pmatrix}
ce^{-2x} & x \geq 0 \\\\
0 & 0 < x \\\\
\end{pmatrix}
$$

### a) Compute the density of X.
From the [joint distributed continuous](#joint-rvs), we can compute the density for X.

$$
f\_X = \int\_{-\infty}^{\infty} f(x,y) dy
f\_X = \int\_{0}^{\infty} xe^{-(x+y)} dy
f\_X = xe^{-x} \int\_{0}^{\infty} e^{-y} dy
f\_X = xe^{-x} \left[ -e^{-y} \right]\_{0}^{\infty} 
f\_X = xe^{-x} (1 - 0)
f\_X = xe^{-x}
$$


$f\_Y = \int\_{-\infty}^{\infty} f(x,y) dx$

### b) Compute the density of Y .

$$
f\_Y = \int\_{-\infty}^{\infty} f(x,y) dx
f\_Y = \int\_{0}^{\infty} xe^{-(x+y)} dx
f\_Y = e^{-y} \int\_{0}^{\infty} xe^{-x} dx
f\_Y = e^{-y} \left[ xe^{-x} \right]\_{0}^{\infty}
f\_Y = e^{-y} (0 - 0)
f\_Y = e^{-y}
$$

### c) Are X and Y independent?
From [independent random values](#ind-rv), we can see if they are independent.

$$
\begin{align}
f(x,y)&=(f\_X(x)f\_Y(y)) \\\\
&= e^{-y} xe^{-x} \\\\
&= xe^{-y-x} \\\\
&= xe^{-(x+y)} \\\\
\end{align}
$$

Therefore yes, they are independent.

## Problem 2.6
A message is transmitted from A to B.
Probability to receive a message is 0.8.
A message is retransmitted until it is successfully received. Let X be a random
variable represented a number of time it took to send the message until it is successfully received.
Find pmf of X.

From [Geometric Distribution](#geo-dis), we can use: $\Pr(X=k)=(1-p)^{k-1}p$, for k = 0, 1, 2, 3, ....

This gives us: $\Pr(X=k)=(1-0.8)^{k-1}0.8$

