# Hypothesis Testing, Part I

# Notes from slides

# Problems

## Problem smm4.1 (Chapter 8, Problem 4)
In a certain chemical process, it is very important that a particular solution that is to he used as a mum have a pH of exactly 8.20.
A method for determining pH that is available for solutions of this type is known to give measurements that are normally distributed with a man equal to the actual pH and with a standard deviation of 0.02.
Suppose 10 independent measurements yielded the following

pH values:

```python
d = [8.18, 8.17, 8.16, 8.15, 8.17, 8.21, 8.22, 8.16, 8.19, 8.18]
```

We make the following null hypothesis

$$
H\_0: \mu = 8.20
$$

Calculate the mean.

```python
barX = sum(d)/len(d)
> 8.179
```

We have the following values:

- $\overline{X} = 8.179$
- $\mu = 8.2$
- $\sigma = 0.02$


Then we find the statistics for known variance, using SMM4S20.

$$
\begin{align}
v &= \frac{\sqrt{n}}{\sigma} \left| \overline{X} - \mu\_0 \right| \\\\
&= \frac{\sqrt{10}}{0.02} \left| 8.179 - 8.20 \right| = 3.32
\end{align}
$$

```python
sigma = 0.02
barX = 8.179
mu = 8.2
v = (sqrt(len(d))/sigma)*abs((barX-mu))
> 3.3203915431766435
```

Now to calculate the p-value, as given on SMM4S21 `matlab p-value = 2*(1-normcdf(v))`

```python
from scipy import stats
p = 2*(1-stats.norm().cdf(v))
> 0.000898912788114492
```

### a) What conclusion can be drawn at the $\alpha = 0.10$ level of significance?
Because our $p < 0.10$, the hypothesis is rejected.

### b) What about at the $\alpha = 0.05$ level of significance?
Because our $p < 0.05$, the hypothesis is rejected.

## Problem SMM4.2 (Chapter 8, problem 7)
Suppose in Problem 4 that we wished to design a test so that if the pH were really equal to 8.20, then this conclusion will be reached with probability equal to 0.95.
On the other hand, if the pH differs from 8.20 by 0.03 (in either direction), we  want the probability of picking up such a difference to armed 0.95.

Hint: It should be interpreted like this: If the real mean value is 8.23 (or 8.17), then the probability that $H\_0$ is accepted should be 0.05.

This means we have a significance level of 0.05.

### a) What test procedure should be used?
In this case we need to consider the errors of type II (accepting $H\_0$ when it is not true).
Hence, now we want to pick the sample large enough such that the probability of accepting $H\_0$ when $\mu \geq 8.23$ or $\mu \leq 8.17$ does not exceed 0.05.

### b) What is the required sample size?
From SMM4S27, we get the following equation, where $\beta$ is defined (in this case) as $1-0.95$.

$$
n \approx \frac{(z\_{\alpha/2} + z\_\beta)^2\sigma^2}{(\mu\_1 - \mu\_0)^2}
$$

```python
zalpha2 = stats.norm().ppf(1-0.025)
zbeta = stats.norm().ppf(1-0.25)
sigma = 0.02
mu1 = 8.17
mu0 = 8.20
((zalpha2+zbeta)**2*sigma**2)/((u1-u0)**2)
> 3.0845984357624308
```

### c) If $\overline{x} = 8.31$, what is your conclusion?
If we use n, from the previous calculations, then we get the following

```python
sigma = 0.02
barX = 8.31
mu = 8.20
n = 3
v = (sqrt(n)/sigma)*abs((barX-mu))
> 9.52627944162893
```

Now to calculate the p-value, as given on SMM4S21 `matlab p-value = 2*(1-normcdf(v))`

```python
from scipy import stats
p = 2*(1-stats.norm().cdf(v))
> 0.0
```

Which is a p-value lower than 0.05, and is therefore rejected.

### d) If the actual pH is 8.32, what is the probability of concluding that the pH is not 8.20, using the foregoing procedure?
