# Presentation of the statistics part of the course

# Notes from slides

# Problems

## Problem 1 (Chapter 2, Problem 29)

Data as a list in python. Put in a zero if you want it to be 1 indexed instead of zero.
But keep in mind length will change etc.

```python
d = [
112, 121, 126, 108, 141, 104, 136, 134,
121, 118, 143, 116, 108, 122, 127, 140,
113, 117, 126, 130, 134, 120, 131, 133,
118, 125, 151, 147, 137, 140, 132, 119,
110, 124, 132, 152, 135, 130, 136, 128
]
```

### a) Compute the sample mean and sample median
Sample mean is given on SMM1S15.

$$
\overline{x} = \frac{1}{n} \sum_{i=1}^n x_i
$$

```python
>>> sum(d)/len(d)
127.425
```

Remember before finding the sample median, to sort the list beforehand (ascending order).

- if n is odd, it is the $\frac{n+1}{2}$-th value
- if n is even, it is the average of $\frac{n}{2}$ and $\frac{n}{2}+1$

We have the even case as we have 40 elements.
Keep in mind I substract 1, becase the list is 0 indexed, therefore the indexes are one number lower.

```python
sd = d
sd.sort()
(sd[20-1]+sd[21-1])/2
> 127.5
```

### b) Are the data approximately normal?
From SMM1S29, it is shown how a approximately normal histogram should look like, putting this into Asymptote shows that it is in fact approximately normal.

```asy
import graph;
import stats;

real[] a={
112, 121, 126, 108, 141, 104, 136, 134,
121, 118, 143, 116, 108, 122, 127, 140,
113, 117, 126, 130, 134, 120, 131, 133,
118, 125, 151, 147, 137, 140, 132, 119,
110, 124, 132, 152, 135, 130, 136, 128
};

size(400,200,IgnoreAspect);
histogram(a,min(a),max(a),10,low=0,lightred,black,bars=false);
xaxis("$x$",BottomTop,LeftTicks);
yaxis("$$",LeftRight,RightTicks(trailingzero));
```

To do this in Python, it can be done as below:

```python
import matplotlib.pyplot as plt
 
d = [ ... valus from earlier ... ]

plt.hist(d, bins=10)
plt.show()
```

### c) Compute the standard deviation s.
From SMM1S17 how to calculate the sample standard deviation is shown.

$$
s = \sqrt{\frac{1}{n-1} \sum_{i=1}^n (x_i - \overline{x})^2}
$$

```python
from math import *

total_sum = 0
sample_mean = sum(d)/len(d)
for i in range(0, len(d)):
    total_sum += (d[i] - sample_mean)**2

s = sqrt(1/(len(d)-1) * total_sum)
s
> 11.873019402982113
```

### d) What percentage of the data fall within $\overline{x} \pm 1,5s$

```python
s = <remember to set this>

within = 0
s_min = sample_mean - s*1.5
s_max = sample_mean + s*1.5
for i in range(0, len(d)):
    dp = d[i]
    if (dp > s_min) and (dp < s_max):
        within += 1
    

within
> 34
100/len(d)*within
> 85
```

Thereby 34 are within the range, and that is equal to 85%.

### e) Compare the answer in part (d) to that given by the empirical rule
From SMM1S29 the empirical rule can be seen, where it is given as

- Approx. 68% of observations lie within $\overline{x} \pm s$
- Approx. 95% of observations lie within $\overline{x} \pm 2s$
- Approx. 99,7% of observations lie within $\overline{x} \pm 3s$

We can easily calculate this from the above, or see that $68% < 85% < 95%$, which confirms the rule.

### f) Compare your answer in part (d) to the bound given by Chebyshev's inequality
TODO(not sure where the formulas come from)

## Problem 2 (Chapter 5, Problem 15)
A club basketball team will play a 60-game season.
32 of these games are against class A teams and 28 are against class B teams.
The outcomes of the games are in-dependent.
The team will win each game against a class A opponent with probability 0.5, and it will win each game against a class B opponent with probability 0.7.
Let X denote its total number of victories in the season.

We can define a function, to show this.

$$
\begin{align}
X_{Ai} = \left\\{
\begin{matrix}
1, \text{if there is a win in thei-th game against the A team} \\\\
0, \text{otherwise}
\end{matrix}
\right.
\end{align}
$$

for $i = 1, \dots, 32$, $X_{Bj}$ is defined similar for $j = 1, \dots, 28$. Which gives us.

$$
\begin{align}
X\_A = \sum\_{i=1}^{32} X\_{Ai} && and &&X\_B = \sum\_{i=1}^{28} X\_{Bj}
\end{align}
$$

### 1. Is X a Binomial random variable?
Look here - https://stattrek.com/probability-distributions/binomial.aspx , the outcome needs to be the same, where for this there is a difference between playing against a A team vs B team.
Therefore X is not a binomial R.V.

### 2. Let $X_A$ and $X_B$ denote, respectively, the number of victories against class A and class B teams. What are the distributions of $X_A$ and $X_B$?
As it can be seen that they respectively denote a binomial distribution, then the values for these are respectively $n=32,p=0.5$ for $X_A$, and $n=28,p=0.7$ for $X_B$. 


### 3. What is the relationship between $X_A$,$X_B$, and X?
The relationship is that the total number of wins, are given as $X=X_A+X_B$.

### 4. Approximate the probability that the team wins 40 or more games.
First we need to calculate the mean and the variance for each $X_A,X_B$, this can be done using what we learned in PMM4, binomial R.V.

$$
\begin{align}
E[X] &= np \\\\
Var(X) &= np(1-p) \\\\
E[X_A] &= 32 \cdot 0.5 &= 16 \\\\
Var(X_A) &= 32 \cdot 0.5(1-0.5) &= 8 \\\\
E[X_B] &= 28 \cdot 0.7 &= 19.6 \\\\
Var(X_B) &= 28 \cdot 0.7(1-0.7) &= 5.88 \\\\
\end{align}
$$

As we have seen the relation is $X=X_A+X_B$, we can add the means together, and as they are independent the variance as well.

$$
\begin{align}
E[X] = 16 + 19.6 = 35.6 \\\\
Var(X) = 8 + 5.88 = 13.88
\end{align}
$$

Now we want to calculate the propability $P[X \geq 40]$, using the example from PMM5S21.

$$
\begin{align}
P[X \geq 40] &= 1 - P[X < 40] = 1 - P \left[ \frac{X - 35.6}{\sqrt{13.8}} < \frac{40 - 35.6}{\sqrt{13.8}} \right] \\\\
&= 1 - P \left[ Z < 1.1810 \right] \text{, Check slide, we substitute here, with Z that is a standard normal variable} \\\\
&= 1 - 0.8810 \text{, Table lookup from PMM5S24} \\\\
&= 0.119 \\\\
\end{align}
$$

## Problem 3 - Look at the solutions

## Problem 4

