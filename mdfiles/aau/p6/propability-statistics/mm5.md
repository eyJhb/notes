# Special random variables (examples of distributions)

# Notes from slides

# Problems


## Problem 5.1 (problem 5.23 from Sheldon Ross, 3rd ed.)
If X is a normal random variable with parameters $\mu = 10$ and $\sigma^2 = 36$, compute

### a) $P(X > 5)$
From MM5S21, we get the following formula

$$
\begin{align}
Z &= \frac{X-\mu}{\sigma} \\\\
P\\{ X < b \\} &= P\\{ \frac{X-\mu}{\sigma} < \frac{b-\mu}{\sigma} \\} = P\\{Z < \frac{b-\mu}{\sigma} \\} = \Phi (\frac{b-\mu}{\sigma})
\end{align}
$$

We can then calculate this by using (note that the range is inverted, therefore subtract it by 1, to get the result):

$$
P\\{ X > 5 \\} = 1 - \Phi (\frac{5-10}{6}) = 1 - \Phi(-0.8333) = 1 - 0,20327 = 0,7967
$$

We get this result by looking it up in a table, we have one for negative values as well as positive ones TODO(insert tables MM5S24 and https://www.ztable.net/wp-content/uploads/2018/11/negativeztable.png) 

### b) $P(4 < X < 16)$
From MM5S21, we get the following formula

$$
\begin{align}
Z &= \frac{X-\mu}{\sigma} \\\\
P\\{ a < X < b \\} &= P\\{ \frac{a-\mu}{\sigma} < Z < \frac{b-\mu}{\sigma} \\} = \Phi (\frac{b-\mu}{\sigma}) - \Phi (\frac{a-\mu}{\sigma})
\end{align}
$$

This gives us:

$$
\begin{align}
P\\{ 4 < X < 16 \\} &= \Phi (\frac{16-10}{6}) - \Phi (\frac{4-10}{6}) \\\\
&= \Phi (1) - \Phi (-1) \\\\
&= 0,8413 - 0,15866 = 0.68264 \\\\
\end{align}
$$

### c) $P(X < 8)$
$$
\begin{align}
P\\{ X < 8 \\} = \Phi (\frac{8-10}{6}) = \Phi(-0.3333) = 0,37070
\end{align}
$$

### d) $P(X < 20)$
$$
\begin{align}
P\\{ X < 20 \\} = \Phi (\frac{20-10}{6}) = \Phi(1,6667) = 0,9525
\end{align}
$$

### e) $P(X > 16)$
$$
\begin{align}
P\\{ X > 16 \\} = 1 - \Phi (\frac{16-10}{6}) = 1 - \Phi(1) = 1 - 0,8413 = 0,1586
\end{align}
$$

## Problem 5.2 (problem 5.25 from Sheldon Ross, 3rd ed.)
The annual rainfall (in inches) in a certain region is normally distributed with $\mu = 40$ and $\sigma = 4$.
What is the probability that in 2 out of the next 4 years the rainfall will exceed 50 inches? Assume that the rainfalls in different years are independent.

We can use what we learned before, and calculate the chance of it raining more than 50 inches.

$$
\begin{align}
P\\{ X > 50 \\} = 1 - \Phi (\frac{50-40}{4}) = 1 - \Phi(2,5) = 1 - 0,9938 = 0,0062
\end{align}
$$

We can then use what we say in Problem 4.2, where we will use $p=0,0062; n = 4; k = 2$..

$$
\begin{align}
P(X = 2) &= 
\begin{pmatrix}
4 \\\\
2
\end{pmatrix}
p^k(1-p)^{n-k} \\\\
&=
\frac{n!}{k!(n-k)!}
\end{align}
= 0,0002277
$$

```python
n = 4
k = 2
p = 0.0062
factorial(n))/(factorial(k)*factorial(n-k)) * p**k*(1-p)**(n-k)
```

## Problem 5.3 (problem 5.28 from Sheldon Ross, 3rd ed.)
A manufacturer produces bolts that are specified to be between 1.19 and 1.21 inches in diameter.
If its production process results in a bolts diameter being normally distributed with mean 1.20 inches and standard deviation 0.005, what percentage of bolts will not meet specifications?

From this we get the following $ P\\{ 1,19 < X < 1,21 \\}; \Mu = 1,20; \sigma = 0,005$.

$$
\begin{align}
P\\{ 1,19 < X < 1,21 \\} &= \Phi (\frac{1,21-1,20}{0,005}) - \Phi (\frac{1,19-1,20}{0,005}) \\\\
&= \Phi (2) - \Phi (-2) \\\\
&=  0,9772 - 0,02275 = 0.9494 \\\\
\end{align}
$$

From this it is seen, that 94,94% are good, while $1-0,9494=0,00505$ are bad.

## Problem 5.4 (problem 4.8 from Sheldon Ross, 3rd ed.)
The time (in hours) required to repair a machine in an exponentially distributed random variable with parameter $\lambda = 1$.

### a) what is the probability that a repair time exceeds 2 hours?
From MM5S09 we can use:

$$
\begin{align}
P \\{ N(t) = k \\} = e^{-\lambda t} \frac{(\lambda t)^k}{k!} \\\\
R(x) = Pr(X > x) = 1 - F(x) = e^{-\lambda x}
\end{align}
$$

$$
P \\{ X > 2 \\} = e^{-2} = 0,1353
$$

### b) what is the conditional probability that a repair takes at least 3 hours, given that its duration exceeds 2 hours?
We can here use a memoryless property, from MM5S11.

$$
P\\{X > s + t | X > t\\} = P(X > s)
= \frac{P\\{ X > s + t, X > t \\}}{P\\{X > t \\}}
= \frac{P\\{ X > s + t\\}}{P\\{X > t \\}}
= \frac{e^{-\lambda (s+t)}}{e^{-\lambda t}}
= e^{-\lambda s}
$$

Here s will be 1, as it is our next step and we need to add 1 to 2, to get 3.
Which then gives us:

$$
P\\{X > s + t | X > t\\} = P(X > s)
= e^{-1} = 0,3679
$$
