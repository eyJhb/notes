# Parameter Estimation and Confidence Intervals

# Notes from slides

# Problems

## Problem smm3.1 (Chapter 7, Problem 8)
An electric scale gives a reading equal to the true weight plus a random error that is normally distributed with mean 0, and standard deviation $\sigma=0.1 mg$.
Suppose that the results of five successive weighings of the same object are as follows: 3.142,  3.163, 3.155, 3.150, 3.141.

### a) Determine a 95 percent confidence interval estimate of the true weight
Using we can do this by searching the z-table body for 0.95, which will give us a z-value of $1.96$, as given to use by the slides as well.
It is possible to get this result from Python as well, as long as your python version is 3.8 or above.

```python
import statistics
statistics.NormalDist().inv_cdf(0.975)
> 1.9599639845400536
```

The reason for using $0.975$ can be found in the link below, but in SMM3S13, bus also in SMM3 - `Handwritten notes about confidence interval`.
Youtube video explaining it - https://youtu.be/9KOJtiHAavE?t=261 and stackoverflow with Python https://stackoverflow.com/questions/20864847/probability-to-z-score-and-vice-versa.

The formula for the confidence interval is the given as:

$$
\left( \overline{x} - z\_{\alpha/2} \frac{\sigma}{\sqrt{n}}, \overline{x} + z\_{\alpha/2} \frac{\sigma}{\sqrt{n}} \right)
$$

Finding the sample mean the (SMM1 problem 1):

```python
d = [3.142,  3.163, 3.155, 3.150, 3.141]
sum(d)/len(d)
> 3.1502
```


Putting that into the formula above, with the $z\_{\alpha/2}$.


```python
xsm-z*(sigma/(sqrt(len(d))))
> 3.0625477459423416
xsm+z*(sigma/(sqrt(len(d))))
> 3.237852254057658
> (3.0625477459423416, 3.237852254057658)
```

### b) Determine a 99 percent confidence interval estimate of the true weight
Calculate the z value again for our 99 percent confidence interval.

```python
import statistics
statistics.NormalDist().inv_cdf((1-0.99)/2+0.99)
> 2.5758293035489
``` 

Putting that in again gives us:

```python
> (3.035005411576574, 3.2653945884234257)
```

Note I use this little thing to calculate it:

```python
confariance = lambda s: xsm+s*z*(sigma/(sqrt(len(d))))
(confariance(-1), confariance(1))
> (3.035005411576574, 3.2653945884234257)
```

## Problem smm3.2 (Chapter 7, problem 9)
The PCB concentration of a fist caught in Lake Michigan was measured by a technique that is known to result in an error of measurement that is normally distributed with a standard deviation of $\sigma = .008 ppm$ (parts per million).
Suppose the results of 10 independent measurements of this fish are:
11.2, 12.4, 10.8, 11.6, 12.5, 10.1, 11.0, 12.2, 12.4, 10.6

### a) Give a 95 percent confidence interval for the PCB level of this fish

```python
d = [11.2, 12.4, 10.8, 11.6, 12.5, 10.1, 11.0, 12.2, 12.4, 10.6]
xsm = sum(d)/len(d)
sigma = 0.008
confariance = lambda s: xsm+s*z*(sigma/(sqrt(len(d))))
(confariance(-1), confariance(1))
> (11.475041639741564, 11.484958360258437)
```
