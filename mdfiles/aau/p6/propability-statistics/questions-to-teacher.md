# Questions
## Problem 2.3
When it is specified BETWEEN or LESS THAN, then why can we take BETWEEN INCLUDING, and not strictly less than etc.? (<= is used instead of <)

## Problem 2.2
Assumptions about lower than and bigger than

## Problem 2.6
Is it correct that it is a geometric series

## Problem 3.4
In the equation for mean and var, it states $1-t$ instead of $t-1$, why is that?
It is wrong when finding $E[X^2]$, as $\frac{1}{(1-0)^3}=\frac{1}{1} = 1$ and not 2.
The equations on the bottom have plus instead of minus, gives the wrong answer even if the right is written down.

## Problem 3.5
Unsure if it is correct, and what the best solution is to use.

## MM4S15 - formula is invalid for Binomial
Should be n/k in the matrix.

## Problem SMM1.5
What are the dots? There is a need for knowing what is meant, to understand what is needed to be calculated.
Why are the dots important?

## Problem SMM3.3
There is nothing about pooled t tests in the slides?

## Problem SMM4.2.b
Seems like the n should be 3.xxxx in this case, how can it be 6?
Also the $\beta$ should be 0.05, not 0.5?

# Myself
There is a need to explain PDF and CDF really really well, a liitle cheat sheet of shorts with all the CDF,PDF, Binomial, etc.
