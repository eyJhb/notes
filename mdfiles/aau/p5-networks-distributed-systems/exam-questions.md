# Exam Questions
## Topic: Time, synchronization and order
### 1. Explain the need for clock synchronisation by giving an example scenario. Provide an example synchronisation approach for 2 nodes and discuss its advantages/disadvantages

#### Example scenario 1
Given multiple computers in a system, if we need to compare logs there is a need for the timestamps to be precise.
If this is not the case, if becomes tedious to compare them.

#### Example scenario 2
If we have a authentication system that provides tickets, e.g. JWT there is a expiration timestamp on the tickets given.
If the server giving out the tickets is behind in time, the ticket could already be expired on the host it were to be given on.
If on the other hand it is forwards in time, the ticket might last longer than expected and could open up for various attacks.

#### Example scenario 3
Having a NAS which is forwards in time, could confuse the clients if they do not agree on the time.
Think of make etc.

#### Synchronization
See [mm1 - clock synchronization protocols](mm1.html#clock-sync-protocols).

### 2. Explain the concept of causal partial order using an example event diagram.
Causal consistency is a weakening model of sequential consistency by categorizing events into those causally related and those that are not. It defines that only write operations that are causally related, need to be seen in the same order by all processes.

This means that there is not a global view of what is happening, and we are therefore dealing with a scalar logical clock (like Lamport timestamps, and not a vector clock).

Now partial order means that we know the partial order of events that caused the event we are looking at.

```
If A and B happen on the same machine and A happens before B then A -> B

If I send you some message M and you receive it then (send M) -> (recv M)

If A -> B and B -> C then A -> C
```

In partial orders we can have events that happen at the same time, which are then independent of each other.

Below is a example with a scalar logical clock.

```asy
unitsize(4mm);
defaultpen(fontsize(20pt)+linewidth(1.5));
usepackage("amsmath");

pair lOff = (0.0,1.5);
void connect(pair p1, pair p2, string p1l, string p2l) {
    // make dots
    dot(p1);dot(p2);

    // make labels
    label(p1l, p1+(lOff));label(p2l, p2+(lOff));

    // connect
    draw(p1--p2,dashed+linewidth(0.6),Arrow);
}

pair cor = (0,0);

int y3 = 0;
draw((0,y3)--(30,y3));
label("P3", (-1.5, y3));

int y2 = 5;
draw((0,y2)--(30,y2));
label("P2", (-1.5, y2));

int y1 = 10;
draw((0,y1)--(30,y1));
label("P1", (-1.5, y1));

// actual stuff
cor = (1,y1);dot(cor);label("1", cor+lOff);
cor = (10,y1);dot(cor);label("4", cor+lOff);
connect((15, y1), (16, y2), "5", "6");

cor = (1,y2);dot(cor);label("1", cor+lOff);
connect((5, y2), (7, y1), "2", "3");
cor = (12,y2);dot(cor);label("4", cor+lOff);
connect((18, y2), (16, y3), "7", "8");

cor = (1,y3);dot(cor);label("1", cor+lOff);
connect((5, y3), (10, y2), "2", "3");
cor = (10,y3);dot(cor);label("3", cor+lOff);
cor = (13,y3);dot(cor);label("4", cor+lOff);
```

Partial order inserted

```asy
unitsize(4mm);
defaultpen(fontsize(20pt)+linewidth(1.5));
usepackage("amsmath");

pair lOff = (0.0,1.5);
void connect(pair p1, pair p2, string p1l, string p2l) {
    // make dots
    dot(p1);dot(p2);

    // make labels
    label(p1l, p1+(lOff));label(p2l, p2+(lOff));

    // connect
    draw(p1--p2,dashed+linewidth(0.6)+red,Arrow);
}

pair cor = (0,0);

int y3 = 0;
draw((0,y3)--(30,y3));
label("P3", (-1.5, y3));

int y2 = 5;
draw((0,y2)--(30,y2));
label("P2", (-1.5, y2));

int y1 = 10;
draw((0,y1)--(30,y1));
label("P1", (-1.5, y1));

// actual stuff
cor = (1,y1);dot(cor);label("1", cor+lOff);
cor = (10,y1);dot(cor);label("4", cor+lOff);
connect((15, y1), (16, y2), "5", "6");

cor = (1,y2);dot(cor);label("1", cor+lOff);
connect((5, y2), (7, y1), "2", "3");
cor = (12,y2);dot(cor);label("4", cor+lOff);
connect((18, y2), (16, y3), "7", "8");

cor = (1,y3);dot(cor);label("1", cor+lOff);
connect((5, y3), (10, y2), "2", "3");
cor = (10,y3);dot(cor);label("3", cor+lOff);
cor = (13,y3);dot(cor);label("4", cor+lOff);

void connectMul(...pair[] elems) {
    if(elems.length < 2){
        return;
    }

    for(int i=1; i < elems.length; ++i) {
        draw(elems[0]--elems[i],dashed+linewidth(0.8)+blue,Arrow);

    }
}

connectMul((1,y1),(5,y2),(5,y3));
connectMul((7,y1),(12,y2),(13,y3));
//connectMul((10,y1),(12,y2),(13,y3));

connectMul((1,y2),(5,y3));
connectMul((5,y2),(10,y3));
connectMul((10,y2),(10,y1),(13,y3));
connectMul((12,y2),(15,y1));


connectMul((1,y3),(5,y2));
connectMul((5,y3),(7,y1));
connectMul((10,y3),(10,y1),(12,y2));
connectMul((13,y3),(15,y1));
```

Total order by using process id to determine ties

```asy
unitsize(4mm);
defaultpen(fontsize(20pt)+linewidth(1.5));
usepackage("amsmath");

pair lOff = (0.0,1.5);
void connect(pair p1, pair p2, string p1l, string p2l) {
    // make dots
    dot(p1);dot(p2);

    // make labels
    label(p1l, p1+(lOff));label(p2l, p2+(lOff));

    // connect
    draw(p1--p2,dashed+linewidth(0.6)+red,Arrow);
}

pair cor = (0,0);

int y3 = 0;
draw((0,y3)--(30,y3));
label("P3", (-1.5, y3));

int y2 = 5;
draw((0,y2)--(30,y2));
label("P2", (-1.5, y2));

int y1 = 10;
draw((0,y1)--(30,y1));
label("P1", (-1.5, y1));

// actual stuff
cor = (1,y1);dot(cor);label("1", cor+lOff);
cor = (10,y1);dot(cor);label("4", cor+lOff);
connect((15, y1), (16, y2), "5", "6");

cor = (1,y2);dot(cor);label("1", cor+lOff);
connect((5, y2), (7, y1), "2", "3");
cor = (12,y2);dot(cor);label("4", cor+lOff);
connect((18, y2), (16, y3), "7", "8");

cor = (1,y3);dot(cor);label("1", cor+lOff);
connect((5, y3), (10, y2), "2", "3");
cor = (10,y3);dot(cor);label("3", cor+lOff);
cor = (13,y3);dot(cor);label("4", cor+lOff);

// total order
draw((1,y1)--(1,y2)--(1,y3)--(5,y2)--(5,y3)--(7,y1)--(10,y2)--(10,y3)--(10,y1)--(12,y2)--(13,y3)--(15,y1)--(16,y2)--(18,y2)--(16,y3),dashed+linewidth(1.1)+green);
```

### 3. Explain the motivation and realization of a logical clock using an example.
The reason for using logical clocks instead of time based clocks is, that there is not the same need for synchronization.
This means that you still know in which order the events occur and what caused what, without having to depend on a time and or timestamps.
Which can be useful when you have a BIG distributed system, with multiple time zones that thereby spans over the entire world.
The reason for this can both be latency, but there can also happen clock skew and generally having a 100% accurate time that matches all other machines in a multi continent setup is not possible.

See the diagram above or below.

### 4. Explain vector clocks and scalar logical clocks using an example and discuss their differences
A logical clock is a clock that is independent on time, but rather implements a common protocol in a distributed context so that all machines can maintain consistent ordering of events in a virtual timespan.

> Given 2 events (e1, e2) where one is caused by the other (e1 contributes to e2 occurring). Then the timestamp of the `caused by` event (e1) is less than the other event (e2).

For a logical clock to provide this, there are two rules.

1. this determines how a local process updates its own clock when an event occurs.
2. determines how a local process updates its own clock when it receives a message from another process. This can be described as how the process brings its local clock inline with information about the global time.

#### Consistency 
Logical clocks are consistent if `a -> b` then `C(a) < C(b)`.
It is strictly consistent if the reverse direction is also true.

#### Scalar Time (Lamport timestamps)
Scalar time is the most simple implementation of logical clocks.
It follows the rules as such.

1. before executing an event (excluding the event of receiving a message) increment the local clock by 1 (`logical_clock += 1`)
2. when receiving a message (the message must include the senders local clock value) set your local clock to the maximum of the received clock value and the local clock value (`logical_clock = max(logical_clock, recv_clock)`). After this, increment your local clock by 1 (`logical_clock += 1`), afterwards the message will become available.

A non-functioning example can be seen below.

```python
class ScalarTime(object):
    def __init__(self):
        self.clock = 0

    def event(self, eventstuff):
        self.clock += 1
        # execute the event

    def send(self, msg):
        self.clock += 1
        # add clock to msg
        # send the message

    def recv(self, msg):
        # get the counter value of the sender
        self.clock = max(self.clock, recv_clock)
        self.clock += 1
        # process message
```

Because they share a counter for internal and world events, it is casually inconsistent.

```plantuml
participant node1 as n1
participant node2 as n2

n1 -> n1: counter = 0
n2 -> n2: counter = 0

group internal event
    n1 -> n1: counter += 1; 1
    n1 -> n1: do some event
end

group external event
    n1 -> n1: counter += 1; 2
    n1 -> n2: counter+msg
    n2 -> n2: recv msg
    n2 -> n2: counter = max(counter, recv_counter)+1; 3
    n2 -> n2: process msg
end
```

See the diagrams above.

#### Vector Clock

1. before executing an event (excluding the event of receiving a message) process Pi increments the value v[i] within its local vector by 1. This is the element in the vector that refers to Processor(i)’s local clock (`local_vector[i] = local_vector[i] + 1`)
2. when receiving a message (the message must include the senders vector) loop through each element in the vector sent and compare it to the local vector, updating the local vector to be the maximum of each element (`for k in range(0,N): local_vector[k] = max(local_vector[k], recv_vector[k])`). Then increment your local clock within the vector by 1 (`local_vector[i] = local_vector[i] + 1`)

A non-functioning example can be seen below.

```python
class VectorClock(object):
    def __init__(self, i, N = 1):
        self.local_vector = [0]*N
        self.i = i # the i of the instance

    def event(self, eventstuff):
        self.local_vector[self.i] += 1
        # execute the event

    def send(self, msg):
        self.local_vector[self.i] += 1
        # add to msg
        # send the message

    def recv(self, msg):
        # update the local vector acording to the received
        for k in range(0, N):
            self.local_vector[k] = max(self.local_vector[k], recv_vector[k])
        self.local_vector[self.i] += 1
        # process message
```

Vector clock is causally consistent, at the price that we sent the vector each time which is costly.

```asy
unitsize(4mm);
defaultpen(fontsize(20pt)+linewidth(1.5));
usepackage("amsmath");

string join(string[] arr, string sep) {
    string finalString;

    for(int i = 0; i < arr.length; ++i) {
        finalString += arr[i];
        if(i != arr.length - 1) {
            finalString += sep;
        }
    }

    return finalString;
}

string[] arrayIntToString(int[] arr) {
    string[] narr;
    for(int i = 0; i < arr.length; ++i) {
        narr.push((string)arr[i]);
    }
    return narr;
}

string[] arrayITS(... int[] elems) {
    return arrayIntToString(elems);
}


pair lOff = (0.5,2.0);
string mArrL(string[] vs) {
    return "$\left[\begin{smallmatrix}" + join(vs, " \\ ") + "\end{smallmatrix}\right]$";
}
string mArrLS(... int[] elems) {
    return mArrL(arrayIntToString(elems));
}
void connect(pair p1, pair p2, string[] p1a, string[] p2a) {
    // make dots
    dot(p1);dot(p2);

    // make labels
    label(mArrL(p1a), p1+(lOff));label(mArrL(p2a), p2+(lOff));

    // connect
    draw(p1--p2,dashed+linewidth(0.6),Arrow);
}

pair cor = (0,0);

int y3 = 0;
draw((0,y3)--(30,y3));
label("P3", (-1.5, y3));

int y2 = 5;
draw((0,y2)--(30,y2));
label("P2", (-1.5, y2));

int y1 = 10;
draw((0,y1)--(30,y1));
label("P1", (-1.5, y1));

// actual stuff
cor = (1,y1);dot(cor);label(mArrLS(1,0,0), cor+lOff);
cor = (10,y1);dot(cor);label(mArrLS(3,2,0), cor+lOff);
connect((15, y1), (16, y2), arrayITS(4,2,0), arrayITS(4,5,2));

cor = (1,y2);dot(cor);label(mArrLS(0,1,0), cor+lOff);
connect((5, y2), (7, y1), arrayITS(0,2,0), arrayITS(2,2,0));
cor = (12,y2);dot(cor);label(mArrLS(0,4,2), cor+lOff);
connect((18, y2), (16, y3), arrayITS(4,6,2), arrayITS(4,6,5));

cor = (1,y3);dot(cor);label(mArrLS(0,0,1), cor+lOff);
connect((5, y3), (10, y2), arrayITS(0,0,2), arrayITS(0,3,2));
cor = (10,y3);dot(cor);label(mArrLS(0,0,3), cor+lOff);
cor = (13,y3);dot(cor);label(mArrLS(0,0,4), cor+lOff);
```

Source - https://levelup.gitconnected.com/distributed-systems-physical-logical-and-vector-clocks-7ca989f5f780

## Topic: Distributed exclusion, consistency
### 5. Explain the distributed mutual exclusion problem and compare it to a centralized approach
#### Distributed Mutual Exclusion
1. requesting process sends request to all other nodes
2. wait until `lock granted` from all nodes (or more than half)
3. when done, unlock message to all nodes

- Problem: Deadlocks, since order of requests may be different at different nodes
- Solution: Introduce total ordering of requests (Lamport timestamps with breaking ties by e.g. process ID)

Also there is Byzantine regarding liars etc.

#### Centralized Approach
- Centralized approach: `lock server`
    - lock request message
    - lock granted reply
    - Advantages
        - More control
        - Avoid "request deadlocks"
    - Disadvantages
        - single-point of failure, possibly loss of information about order of requests after crash
        - failure of client that holds lock leads to deadlock
        - performance aspects: high load on lock server

### 6. Give an example of a distributed read and write operation sequence and explain two different consistency criteria.
For optimal performance, a system might read more than it writes and in those cases it makes sense to allow multiple nodes to read at the same time.
There will be given a example of how this is done.

#### Write
The node that wants to write requests the lock from all the other nodes, when at least 50% reply OK, it will use the lock to write on the resource in question.
When it is done, it will send out a message releasing the lock.

#### Read
The node that wants to read requests a read lock from all the other nodes, when at least 50% reply OK it has a lock to read the resource.
When it is done reading then it will release the lock again.
However, if we want multiple instance that can read at the same time, then if a node already has the read lock a second node can be granted a read lock as well, UNLESS there is a write node in queue of getting a write lock.
So now there is a need for a FIFO queue of some kind.

#### Consistency
##### Linearizability
Means that no two operations happens at the same time, but they happen in the real time order of events received (requires global synced time).
The sequential order is consistent with the real-time ordering of the distributed events (operation a happens before operation b if a finishes before b starts).

- If there exists a sequential event order leading to the same results and
- The sequential order is consistent with the real-time ordering of the distributed events (operation a happens before operation b if a finishes before b starts)

Example can be seen below.

```asy
unitsize(3mm);

// init
int y3 = 5, y2 = 10, y1 = 15;

// time
draw(L=Label("Time", align=S),(0,0)--(50,0),linewidth(1.5),Arrow);

// labels
label("P1", (0, y1),fontsize(24pt));
label("P2", (0, y2),fontsize(24pt));
label("P3", (0, y3),fontsize(24pt));

// event P1 stuff
draw(L=Label("$w_1(x)a$",align=N),(5,y1)--(10,y1),Arrows);
draw(L=Label("$r_1(x)b$",align=N),(15,y1)--(20,y1),Arrows);

// event P1 stuff
draw(L=Label("$w_2(x)b$",align=N),(12,y2)--(30,y2),Arrows);
draw(L=Label("$r_2(x)c$",align=N),(35,y2)--(45,y2),Arrows);

// event C stuff
draw(L=Label("$w_3(x)c$",align=N),(22,y3)--(28,y3),Arrows);
```

##### Serializability
To create a Serialized history of these events, one has to separate all the operations of the transactions A, B and C so there are no interleaved operations from other transactions.
This would mean that each process has it order, which is executed on the server in that order.

- If there exists a sequential event order leading to the same results and
- The sequential order is consistent with the ordering of the events at each node

Example below:

```asy
unitsize(3mm);

// init
int y3 = 5, y2 = 10, y1 = 15;

// time
draw(L=Label("Time", align=S),(0,0)--(50,0),linewidth(1.5),Arrow);

// labels
label("P1", (0, y1),fontsize(24pt));
label("P2", (0, y2),fontsize(24pt));
label("P3", (0, y3),fontsize(24pt));

// event P1 stuff
draw(L=Label("$r_1(x)b$",align=N),(20,y1)--(30,y1),Arrows);

// event P1 stuff
draw(L=Label("$w_2(y)c$",align=N),(15,y2)--(24,y2),Arrows);
draw(L=Label("$w_2(x)b$",align=N),(26,y2)--(40,y2),Arrows);

// event C stuff
draw(L=Label("$w_3(x)a$",align=N),(5,y3)--(13,y3),Arrows);
draw(L=Label("$r_3(x)a$",align=N),(42,y3)--(50,y3),Arrows);
```

##### General example here:
```asy
unitsize(3mm);

// init
int yc = 5, yb = 10, ya = 15;

// time
draw(L=Label("Time", align=S),(0,0)--(50,0),linewidth(1.5),Arrow);

// labels
label("A", (0, ya),fontsize(24pt));
label("B", (0, yb),fontsize(24pt));
label("C", (0, yc),fontsize(24pt));

// event A stuff
draw(L=Label("$r(x)$",align=N),(5,ya)--(15,ya),Arrows);
draw(L=Label("$w(x)$",align=N),(40,ya)--(50,ya),Arrows);

// event B stuff
draw(L=Label("$r(x)$",align=N),(10,yb)--(25,yb),Arrows);
draw(L=Label("$r(y)$",align=N),(30,yb)--(42,yb),Arrows);

// event C stuff
draw(L=Label("$r(y)$",align=N),(43,yc)--(48,yc),Arrows);
```

- For Linearizability the order is: A.r(x), B.r(X), B.r(Y), A.w(X), C.r(Y)
- For Serializability the order is: A.r(x), A.w(x), B.r(X), B.r(Y), C.r(Y)

Read this - http://www.bailis.org/blog/linearizability-versus-serializability/

Source: https://stackoverflow.com/a/8299001

## Topic: Techniques used at Data Link layer: ARQ and MAC concepts
### 7. Explain the three types of ARQ protocols.
ARQ means `Automatic Repeat Request` and is a error control method for data transmission that uses acknowledgments and timeout to reliably transmit data.

#### Stop-and-wait protocol
- After a transmission of a frame, the transmitter waits for ACK/NACK. If NACK is received (or time-out), the frame is retransmitted.
- Higher data rates and utilization of satellite channels with long round-trip delays led to the need for continuous transmission strategies. 

#### Go-Back-N
The Go-Back-N will restart sending the sequence on timeout, or a NACK as can be seen in the examples.
So if A1, A2, A3, A4 was sent, and there is a ACK1, ACK2 and a timeout, then it will resend from A3.

```
Deterioration of throughput performance if large round-trip delays are involved.
```

##### NACK example
| A1   | A2   | A3    | A4 | A5 | A6 | A7 | A3   | A4   | A5   |
|------|------|-------|----|----|----|----|------|------|------|
| ACK1 | ACK2 | NACK3 |    |    |    |    | ACK3 | ACK4 | .... |

##### Timeout example
| A1   | A2   | A3 | A4 | A5 | A6 | A7 | A3   | A4   | A5   |
|------|------|----|----|----|----|----|------|------|------|
| ACK1 | ACK2 |    |    |    |    |    | ACK3 | ACK4 | .... |

#### Selective repeat procedure
Only the frame negatively acknowledge need be retransmitted -> buffer at the receiver side for storing correctly received packets + reordering of frames in the receiver buffer is required.

- Handling buffer overflow:
    - To prevent buffer overflow
    - To retransmit the packets that are lost due to the buffer overflow 

This means, that if Go-Back-N is used as a example, it will not resend the entire series, but instead only the packets that are not ACK'ed.

### 8. Give examples of MAC protocols with static channel allocation. Discuss their advantages and disadvantages.
Good visual example here - https://www.researchgate.net/figure/Illustrative-example-of-different-multiple-access-schemes-a-TDMA-b-FDMA-c-OFDMA-d_fig4_323141497 (recreate in asymptote)

### SDMA (Space Division Multiple Access)
SDMA will divide the MAC into physical space called sectors and use directed antennas for this.
This is what was used early as the basis for cellphone structure.

#### Pros
- Stable

#### Cons
- Requires multiple directed antennas
- Only works with antennas? (unless you dedicate a cable to each user?)

### TDMA (Time Division Multiple Access)
TDMA allows multiple users to use the same frequency by dividing it into timeslots.
Each user transmits when they have their allocated timeslot and will stop once it is not their turn anymore.

#### Pros
- Frequency stability is not as big a need as in FDMA, as there is a larger range to work with

#### Cons
- Requires synchronisation

### FDMA (Frequency Division Multiple Access)
FDMA divides the bandwidth into equal frequencies that each user can transmit on, including a buffer between each frequency.

#### Pros
- Users can transmit at the same time
- No need for synchronisation

#### Cons
- Lower bandwidth pr. user
- Waste in buffer
- More likely to have interference

Good visual representation - https://www.researchgate.net/figure/Illustrative-example-of-different-multiple-access-schemes-a-TDMA-b-FDMA-c-OFDMA-d_fig4_323141497 

### 9. Explain main features of Carrier Sense Multiple Access (CSMA) protocol, including the difference between non-persistent and 1-persistent versions.
A transmitter attempts to determine whether another transmission is in progress before initiating a transmission using a carrier-sense mechanism.
That is, it tries to detect the presence of a carrier signal from another node before attempting to transmit.
If a carrier is sensed, the node waits for the transmission in progress to end before initiating its own transmission.
Using CSMA, multiple nodes may, in turn, send and receive on the same medium.
Transmissions by one node are generally received by all other nodes connected to the medium.

### Non-persistent
Non persistent CSMA is a non aggressive transmission algorithm.
When the transmitting node is ready to transmit data, it senses the transmission medium for idle or busy.
If idle, then it transmits immediately.
If busy, then it waits for a random period of time (during which it does not sense the transmission medium) before repeating the whole logic cycle (which started with sensing the transmission medium for idle or busy) again.
This approach reduces collision, results in overall higher medium throughput but with a penalty of longer initial delay compared to 1–persistent.

### 1-persistent
1-persistent CSMA is an aggressive transmission algorithm.
When the transmitting node is ready to transmit, it senses the transmission medium for idle or busy.
If idle, then it transmits immediately.
If busy, then it senses the transmission medium continuously until it becomes idle, then transmits the message (a frame) unconditionally (i.e. with probability=1).
In case of a collision, the sender waits for a random period of time and attempts the same procedure again.
1-persistent CSMA is used in CSMA/CD systems including Ethernet.

### 10. Explain the hidden terminal problem. What are the possible solutions to overcome it?
The hidden terminal problem is if you have three nodes, A, B and C. A can see B, and C can see B, but A cannot see C.
Therefore A and C cannot sense when one another are transmitting to B, and will therefore result in invalid data being received.
This can be seen below:

```asy
unitsize(4mm);

void drawAnt(int x, pen color, string name) {
    draw(circle((x,10),7),dashed+linewidth(1.5)+color);
    draw((x,10)--(x,0));
    label(name,(x, 11));
}
drawAnt(0, red, "A");
drawAnt(6, green, "B");
drawAnt(12, blue, "C");
```

#### Solutions
- Increasing transmitting power - allows A and C to see each other
- Moving the node
- Protocol Enchancement - TDMA or MACA can also be used, with RTS/CTS (request-to-send/clear-to-send)

#### Exposed Terminal Problem
There is also the exposed terminal problem:
In wireless networks, the exposed node problem occurs when a node is prevented from sending packets to other nodes because of co-channel interference with a neighboring transmitter. Consider an example of four nodes labeled R1, S1, S2, and R2, where the two receivers (R1, R2) are out of range of each other, yet the two transmitters (S1, S2) in the middle are in range of each other. Here, if a transmission between S1 and R1 is taking place, node S2 is prevented from transmitting to R2 as it concludes after carrier sense that it will interfere with the transmission by its neighbor S1. However note that R2 could still receive the transmission of S2 without interference because it is out of range of S1.

```asy
unitsize(3mm);

void drawAnt(int x, pen color, string name) {
    draw(circle((x,10),14),dashed+linewidth(1.5)+color);
    draw((x,10)--(x,0));
    label(name,(x, 11));
}

drawAnt(0, red, "R1");
drawAnt(12, blue, "S1");
draw(L=Label("Transmitting"),(11.5,10)--(0.5,10),Arrow);
drawAnt(24, orange, "S2");
drawAnt(36, green, "R2");
draw(L=Label("Waiting",align=N),(24.5,10)--(35.5,10),Arrow);
```

## Topic: WLAN IEEE 802.11 standard
### 11. Explain the Random backoff time mechanism used in the standard.
If multiple stations are waiting for the medium to become available -> potential for repeated collisions

- To break symmetry: randomization
    * Each station randomly chooses integer counter value in between 0 and CW (Contention Window)
    * when medium was idle for a slot-time back-off counter is decreased
    * Transmission only started when counter=0 and medium idle
- Contention Window Size
    * Initial setting: CW=7
    * When Collisions detected (missing ACKs) -> CW is doubled
    * After successful transmission -> CW set back to initial value

### 12. Explain the “handshaking” mode of operation (with RTS/CTS messages) and basic mode (without RTS/ CTS) and the associated trade-offs.
When a node wants to transmit, it sends a signal called Request-To-Send (RTS) with the length of the data frame to send. If the receiver allows the transmission, it replies the sender a signal called Clear-To-Send (CTS) with the length of the frame that is about to receive.
Meanwhile, a node that hears RTS should remain silent to avoid conflict with CTS; a node that hears CTS should keep silent until the data transmission is complete.

So from before we have nodes, A, B and C. A cannot see C and wise versa, but A and C can see B.
A and C wants to send to B, therefore the both send a RTS.
A sends it first, and gets a CTS from B, now C is silent because it heard the CTS from B.
The data is sent, and it can start over.

This does however not prevent the exposed terminal problem.

```
WLAN data transmission collisions may still occur, and the MACA for Wireless (MACAW) is introduced to extend the function of MACA. It requires nodes sending acknowledgements after each successful frame transmission, as well as the additional function of Carrier sense.
```

- Advantages
    - Collisions are avoided
    - Hidden terminal problem is fixed
- Disadvantages
    - Bandwidth reduction (overhead)
    - No multicast/broadcast

#### Frames
- RA - Receiver Address indicating the MAC address of the station that shall receive frame.
- TA - Transmitter Address indicating the MAC address of the station which has transmitted frame.
- FCS - Frame Check Sequence.

##### RTS frame
- Frame Control
- Duration
- RA (Receiver Address)
- TA (Transmitter Address)
- FCS

##### CTS frame
- Frame Control
- Duration
- RA (Receiver Address)
- FCS

##### ACK frame
- Frame Control
- Duration
- RA (Receiver Address)
- FCS

## Topic: Bluetooth
### 13. Explain data exchange in a piconet (polling; synchronization; slot structure; packet types)
#### Background information
- BT applies per-packet frequency hopping for transmitting data over the air
    - Basic physical channel is defined by pseudorandom hoping over 79 RF channels, 1 MHz carrier spacing
    - Channel 0: 2402 MHz to channel 78: 2480 MHz
    - Frequency hopping with 1600 hops/s
- Each piconet has a unique hopping pattern
- Participation in a piconet = synchronization to hopping sequence
- Master gives slaves its clock and device ID
    * Hopping pattern: determined by device ID (48 bit, unique worldwide)
    * Phase in hopping pattern determined by clock
    * Hopping sequence in a pseudo random fashion, determined by a master
- Addressing
    * Active Member Address (AMA, 3 bit)
    * Parked Member Address (PMA, 8 bit) 
- Labels
    * SB: standby
    * M: master
    * S: slave
    * P: parked

#### Polling
Polling-based TDD (Time Division Duplex) packet transmission

- 625µs slots, master polls slaves
    * Master polls slaves according to a polling scheme.
    * Slave transmits only after it has been polled (NULL packet)
    * Master schedules the traffic in both the uplink and downlink -> completely contention-free access -> intelligent scheduling algorithms are needed 

#### Synchronization
- Participation in a piconet = synchronization to hopping sequence
- Master gives slaves its clock and device ID
    * Hopping pattern: determined by device ID (48 bit, unique worldwide)
    * Phase in hopping pattern determined by clock
    * Hopping sequence in a pseudo random fashion, determined by a master

#### Slot Structure
- The Bluetooth transceiver uses a time-division duplex (TDD) scheme: RX and TX packets are sent alternating, at beginning of each 625µs time-slot.
    * Settling time due to frequency hopping is 250-260µs per hop. (To allow radio circuits to stabilize.)
- Thus, for a single slot frame, the maximum length is 366µs.
    * For multi-slot frames only a single settling time is used (thus allowing for higher throughput).
- A master shall always start to transmit in odd time-slots, whereas the slave addressed transmits during even time-slots. (A slave can only answer to a master, and cannot initiate a conversation).

#### Packet Types
- ID (identification packet) - used for signalling (but not listed as it has no header)
- NULL - consists of access code +header (feedback, no ACK)
- POLL - used by a master to force a slave to return a response (feedback, with ACK)
- FHS (FH synchronisation) - used to exchange clock and ID information between units
- DM1 – special, can carry payload

## Topic: Field bus' - CAN and FlexRay
### 14. Outline the differences between CAN and FlexRay in relation to Ethernet in terms of:
### Addressing (how are frame ids used)
- How is it compared to CAN?
    * Flexray uses 12 bits frame IDs vs. 11 bits IDs in CAN
    * Frame ID relates to message and is used as priority (only in dynamic segment for flexray)
    * CAN only 0-8 bytes, but payload size can always vary. FlexRay payload size is only variable in dynamic segment.
    * CAN has no cycle - it is purely event-based. (not fit for periodic signals or large throughput
    * FlexRay has both evens and cycles
- How is it compared to Ethernet?
    * Ethernet is 0-1500 bytes, variable.
    * Addresses through layer 2 (MAC) -  Dest. address instead of frame ID or broadcast
    * Ethernet has neither cycles or priorities

### Network topologies
- How is it compared to CAN?
    * Bus segments can be connected with AS. CAN requires GW with re-arbitration on each bus.
    * Bus segment length shorter than CAN (Due to higher bit-rate).
- How is it compared to Ethernet?
    * AS replicates all data streams to all ports (switch only forwards to destination address)
    * Ethernet (CAT5) has maximum length of 100 m.
TODO(image of topologies?)

### Typical usage scenarios and their requirements to payload and bit rates, as well as overhead/efficiency
- FlexRay
    - High data rates (up to 10 Mbit/s)
    - Fit for both periodic and event based behaviour
        - Time- and event-triggered behaviour
    - Fault-tolerance (Bit-coding, clock-sync)
    - Deterministic
- CAN
    - Slower than FlexRay (Bottle-neck problem) at data rates up to around 1 Mbit/s
    - Cheap to wire physically
    - Simple implementation
    - Payload data size can always vary.
    - CAN payload can vary in size, whereas FlexRay only varies when it is dynamic.
- Ethernet
    - Ethernet supports much higher throughput (data rate) of up to 400 gigabit/s
    - Ethernet has a destination ID, and varies greatly in size but the throughput it much faster.

### 15. Explain the role of the frame ID in the priority arbitration scheme used in CAN. Show an example of bit sequences of two frames with different IDs that are transmitted at the same time.
Basically, the lower the frame ID the higher the priority.
This means, that if two frames with different IDs are transmitted concurrently, the one with the lowest ID
will win priority and get send. Below is an illustrated example of the course of such a transfer

```
Bits1 = 111010110110
Bits2 = 111010111110
```

Once they reach the bit where bits2 is still high, while bits1 is low bits2 will be dropped.

TODO(insert Jacob image)

### 16.Explain the medium access control scheme in the dynamic segment of Flexray. Explain in which cases a message is guaranteed to be transmitted.
- The dynamic segment uses a dynamic minislotting scheme for medium access.
- Prioritized access until segment ends.
- Variable frame sizes.

MAC is based on the priority of IDs in the dynamic segment, but TDMA in the static segment.
If we have 40 minislots, and our current message has the highest priority, e.g. 1, then as long as the length of the message is less than or equal to 40, then it is guaranteed to be sent.

### 17.Discuss pro/con of assigning messages in FlexRay static segment vs. dynamic segment. When would you use either segment for:
Static Segment example: continuous sampling of tire position for the ABS system in a car (periodic, low throughput, low jitter).
Dynamic segment example: turning on the signal in a car which is activated by user request (event, low throughput (could be high though), low/high jitter).

#### a) Periodic/event based messages.
Periodic based messages should normally be scheduled in the static segment
for maximum reliability. Usually these periodic messages consist of sensor
update signals or similar, that are not huge and that are consistently sent
over a time span. Event based messages are more fit for dynamic segments,
as only the size of 1 mini slot is lost in the dynamic segment if an event is not
triggered throughout a cycle, whereas in the static segment an entire static
period is lost.

#### b) High/low delay or jitter requirements.
If a signal/message requires low jitter or delay it should usually be places in
the static segment, where there is no variation in what gets sent or not sent
in every cycle. If a signal/message is okay with delay or jitter is can be placed
in the dynamic segment with a high slot-priority number.

#### c) High/low throughput.
High throughput should be placed in the dynamic segment with low slotpriority number. This would result in the message being guaranteed to be
send in every dynamic cycle where it is activated. Low throughout should
either be placed in the static segment, if it corresponds to the frame sizes.
Otherwise in the dynamic segment with higher slot-priority number.

### 18. A Flexray bus should carry periodic signals with cycle times of 10ms, 3ms, and 4ms. What cycle time would you choose when scheduling these signals in the static segment of Flexray? Give reasons for your choice and discuss advantages and disadvantages. What cycle time and frame IDs would you use when scheduling those in the dynamic segment?
Signal every 3rd, 4th and 10th ms
For this kind of problem, one can lower the total period length to 1ms as an
example. This means that the slots dedicated to each signal will only be used
every 3rd 4th and 10th cycles. While this does introduce overhead (as most of the
time the slots will not be used), it is necessary to ensure timely measurements. One
can however use multiplexing to mitigate this overhead somewhat. The simplest
way to do this, is by having several signals (with the same periodicity) share a
given slot.

- advantage: ensures timely measurements is simple to implement
- disadvantage: overhead if not multiplexed properly short periods create small dynamic segments

## Topic: Introduction to Security
### 19.Encryption: why is it used? Explain the difference between symmetric and asymmetric encryption mechanisms. Explain the difference between a block-cipher and a stream-cipher. Give an example of an encryption algorithm. 
#### Why is it used?
Encryption is used to make data private, ensure you are talking with the correct party, to ensure no tampering of data, etc.

#### Symmetric and asymmetric encryption
Symmetric encryption is when a single key is shared to both encrypt and decrypt the ciphertext.

Asymmetric encryption has a separate key for encryption and decryption, a good example is public/private keys in the form of HTTPS.

#### Block Cipher and stream-cipher
Block cipher encrypts blocks at a time, e.g. 1024 bits of data and then reuses the 1024 bit key once again for the next block.

Stream cipher encrypts a single bit at a time, and the key used needs to be the same length as the data you want to encrypt.

#### Algorithms
- DES (Data Encryption Standard) - Symmetric
- RSA (Rivest-Shamir-Adleman) - Asymmetric
- AES (Advanced Encryption Standard) - Asymmetric

### 20.Integrity protection: what is a purpose of it? How can it be achieved?
Integrity protection can be achieved using encryption, if you can ensure that no one can decrypt and encrypt it once more.
The purpose is that people cannot tamper with the sent data, e.g. disabling sensors.

This can also be done by using a hash, that is shared in a secure way - so this is without encryption.

It is important to note, that without further protection than encryption, replay attacks are still possible.

### 21.Explain the principles of challenges-response mechanism for authentication. Show an example realization as message sequence chart.
- Symmetric authentication
    - Based on shared secret: Key K
    - Problem: Key K must not be transmitted over channel in the authentication protocol
    - Solution: Challenge-response methods
        - Scenario: B authenticates at A
            - A picks random number *rand* (the challenge) and sends to B
            - B computes cryptographic one-way function $y=f(rand,K)$ and sends y to A, e.g. exponentiation $y = rand^K mod n$
            - A verifies whether the received y equals $f(RAND,K)$
- Asymmetric authentication
    - Server A wants to authentication user B
    - Server sends public cert to B
    - B encrypts using public cert send to A
    - A can now decrypt and see it matches

```plantuml
participant A as a
participant B as b
a -> a: generate random number R
a -> b: send R
b -> b: one-way function y=f(R,K) e.g. hash or exponentiation
b -> a: sends y
a -> a: compares y to f(R,K) itself
```

### 22.Explain the purpose of key agreement protocols. Give an example realization of a key agreement protocol and discuss its strengths and weaknesses.
The purpose is to have both parties participate in making a key, where any eavesdroppers wont get any knowledge of the final key.

Diffie-Hellman is used in this example.

- Alice and Bob agree on a prime number *p* (23) and integer *g* (5) (can be publicly known)
- Alice selects a secret *a* (4), and the send Bob $A=g^a mod p$ (4)
- Bob selects a secret *b* (3), and the send Alice $B=g^b mod p$ (10)
- Alice then computes $s = B^a mod p$ (18)
- Bob then computes $s = A^b mod p$ (18)
- *s* is then the shared secret they can use

Anonymous key agreement protocols are vulnerable to man-in-the-middle, whereas public/private are not.

## Topic: Introduction to Fault Tolerance
### 23. A server node shows has a down-time of 20 hours per year. Calculate the resulting availability Pr(Server operational)
From slides

```
Metrics – assuming binary system state (up/down)
* Availability (’accessibility’) A(t)
  - Pr(System operational at time t)
  - request based: Pr(System accepts a service request at time t)
* Reliability R(t_1 ,t_2)
  - Pr(System operational within time interval [t_1 ,t_2] provided it was operational (’accessible’) at time t_1)
  - Request-based: Pr(System serves a request given that it had accepted it at time t_1)
```

So I would just calculate it as $365 \cdot 24=8760$, $(100/8760) \cdot (8760-20)=99.77%$ uptime.

### 24.A redundant structure of 3 servers is used. Calculate its availability assuming independent faults.
For independent components: A=1-PI(1-Ai) Assuming that the server has a 96% up-time and that all the server are identical the following calculation can be made.
If the first server has a reliability of 95% and the other servers are identical then the second system will cover 95% of the first systems downtime which is 5%, so 95% of 5% = 4.75%
The two clustered machines, if clustered properly, and if there are no calamities that affect both systems, should be available 95 + 4,75 = 99,75% of the time.
Then adding in the 3. server it will likewise do the same and cover 95% of 0,25% which is = 0,2425 So all in all it will have an uptime of 99,75 + 0,2425 = 99,9925%

If they are redundant setup, and it is independent faults there should be no downtime in general.

### 25.Discuss advantages and disadvantages of cluster structures (that hide the redundancy to accessing nodes) as opposed to an architecture where failover is done via the Clients (such as RSerPool)
If done internally and hiding the redundancy, the cluster adminitrator chooses which servers the should failover too, whereas in the other case the clients decide.
This can end up with all the servers becoming unavailable, and starting a evil circle of unavailable services.

Also the list would probably be hardcoded in the clients, and therefore hard to change.

### 26.Explain the setting and goal of a distributed consensus algorithm. Outline an example approach for a setting without faults, and explain how byzzantine faults complicates this setting (why your given algorithm may not work).
The consensus problem requires agreement among a number of processes (or agents) for a single data value.
Some of the processes (agents) may fail or be unreliable in other ways, so consensus protocols must be fault tolerant or resilient.
The processes must somehow put forth their candidate values, communicate with one another, and agree on a single consensus value.
The consensus problem is a fundamental problem in control of multi-agent systems.
One approach to generating consensus is for all processes (agents) to agree on a majority value.
In this context, a majority requires at least one more than half of available votes (where each process is given a vote).
However, one or more faulty processes may skew the resultant outcome such that consensus may not be reached or reached incorrectly.
In a Byzantine system where nodes have different incentives and can lie, coordinate, or act arbitrarily, you cannot assume a simple majority is enough to reach consensus.
Half or more of the supposedly honest nodes can coordinate with each other to lie.
Trying to build a reliable computer system that can handle processes that provide conflicting information is formally known as the "Byzantine General’s Problem"
A Byzantine fault-tolerant protocol should be able to achieve its common goal even against malicious behavior from nodes.

- N nodes ’Generals’, m out of these are faulty (’traitors’), N-m loyal
- Each node i has node-specific information v(i)
- Goal: After message exchange, each loyal node should have identical vector u with
    - u(j)=v(j), if node j is loyal
    - u(j) arbitrary value if node j is traitor
- It is not known before, which nodes are faulty/traitors
- Assumptions on message exchange
    - Every node can communicate directly with every other node (fully meshed topology)
    - Every message sent is delivered correctly
    - Receiver of a message knows who sent it
    - Absence of a message can be detected
    - Message content is in complete control of sender (no forwarding of signed messages)

The problem is solvable for $N \geq 3*m+1$, e.g. N=3 nodes and 1 traitor is not solvable, but for N=4 nodes and 1 traitor is.

## Topic: TCP Performance
### 27.Explain the purpose of TCP’s congestion window (CWND) in comparison to the receiver advertised window (RWND).
TCP uses an end-to-end flow control to ensure that the data transfer is not too fast using a sliding window flow control protocol.
The receiver sets the RWND which indicates that the receiver can buffer that much data, before the sender must wait for an ACK.
The RWND is communicated back to the sender in each ACK it receives.
The CWND is set by the sender and is used to ensure that there is no network congestion (e.g. a router on the way is overloaded).

A example of this could be:

If the window limit is 10 packets then in slow start mode the transmitter may start transmitting one packet followed by two packets (before transmitting two packets, one packet ack has to be received), followed by three packets and so on until 10 packets. But after reaching 10 packets, further transmissions are restricted to one packet transmitted for one ack packet received. In a simulation this appears as if the window is moving by one packet distance for every ack packet received. On the receiver side also the window moves one packet for every packet received.

It starts by being set to one and then it increases for each acknowledgement received (as this is equal to the number of packets sent, it doubles the window for each round trip). This happens until the ssthresh (Slow Start Threshold) is reached. When this happens, slow start is stopped and congestion avoidance begins. The congestion avoidance is a linear increase to the congestion window for each round trip.

### 28.An end-to-end TCP connection shows a round-trip time of 1s. Segment Size is 1kB. Receiver-Advertised Window is 16kB (16 segments). The bottleneck link is a wireless link with 100kbit/s. Assume that the congestion window is larger than the receiver advertised window. What is the maximum achievable throughput of the TCP connection? Is the round-trip time or the bandwidth limitation the limiting factor in this setting?
- Round-trip time = 1s
- Segment Size = 1kB
- Receiver-Advertised Window = 16kB
- Bandwidth = 100 kbit/s

$$
\begin{align}
bandwidth = \frac{100 kbit/s}{8 kbit/kB} = 12,5 kB/s \\\\
transmissiontime = \frac{16 kB}{12,5 kB/s} = 1,28s \\\\
throughput = \frac{16 kB}{1s} = 16 kB/s \\\\
\end{align}
$$

It is the bandwidth, as the RTT is 1s, it it was 1,28s they would be equal and if it was bigger than 1,28s then it would be the RTT that was the limiting factor.

### 29.Explain the possible problems of TCP when used over wireless access links or over low-bandwidth technologies like narrow-band PLC. Sketch one solution approach for improvement in this scenario.
The TCP assumes that network congestion is responsible for any packets dropped on the network.
This works well on wired networks, but not on wireless ones.
On wireless networks dropped packets are commonly due to interference on the physical layer.
It can also be due to handovers between different routers (or celltowers etc.) All of these different packet loss scenarios lowers the efficiency of the TCP.
For low bandwidth this might be timeouts happening as well.

#### Possible fixes
A little info on Snoop - The Snoop protocol works by deploying a Snoop agent at the base station and performing retransmissions of lost segments based on duplicate TCP acknowledgments (which are a strong indicator of lost packets) and locally estimated last-hop round-trip times. The agent also suppresses duplicate acknowledgments corresponding to wireless losses from the TCP sender, thereby preventing unnecessary congestion control invocations at the sender.

##### Link layer solution
This solution would be to make the linklayer responsible for retransmission of packets lost over the wireless connection.
This would hide the packet loss from the TCP and would therefore not incur these great performance degredations.

##### Split connection approach
This approach uses a proxy inbetween the two hosts, with only the wireless part running from the proxy to the wireless end.
The other host would be on a normal wired TCP connection to the proxy.

##### Explicit notification approach
This approach would introduce the capability to notify the other host about the presence of a wireless link in the communication chain.

##### End to End approach (Stack at Sender and Receiver enhanced)
Finally, the TCP could be enhanced.
This could be no slow starts, detecting multiple loses etc.
This would be beneficial in that end to end encryption, such as IPsec, would still be available.
The drawback is, that the end host (on a wired network) would also need to have this enhanced TCP update.

Best solution: SACK + Snoop, which means a combination of the End to End approach and the Link Layer approach

Snoop means that e.g. the access point buffers the packets, and will answer to NACK, etc. and generally filter traffic

## Topic: Localization
### 30.Explain triangulation and trilateration principle.
Triangulation is based on knowing the angles A and B, plus the length |AB|, thereby you can get the exact location of the user.

Trilateration is based on knowing the distance to the user, and thereby drawing circles around each station, so that where they cross is where the user is located.

TODO(insert graphs)

## Topic: Routing in ad hoc networks
### 31.Explain the difference between proactive and reactive routing approaches.
#### Proactive routing
- Keeps track of routes for all destinations
- Each node maintains tables for routing information
- Changes in the topology is handled by propagating route updates
    - Periodic
    - Event-triggered

#### Reactive routing
- Routing information acquired when needed
- Initialization of route discovery
- Maintain routes only if needed

#### Pros/Cons
- Latency
    - Proactive may have lower latency (always have the data)
    - Reactive because it will only find the route on request
- Overhead of route discovery/maintenance 
    - Proactive may result in higher overhead (continuous update of routes)
    - Reactive may have lower overhead (updated on a new basis)

| Routing class                | Proactive                          | Reactive                                           | Hybrid                                                                                                  |
|------------------------------|------------------------------------|----------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| Routing Structure            | both flat and hierarchical         | Mostly flat                                        | Mostly hierarchical                                                                                     |
| Availability Routing         | Always available                   | Determined when needed                             | Depends on the location of the destination                                                              |
| Control of route             | Usually high                       | Lower than proactive                               | Mostly lower                                                                                            |
| Periodic                     | yes                                | Not required                                       | Usually used inside each zone or between gateways                                                       |
| Handling effects of mobility | usually updates at fixed intervals | Sometimes threshold updates                        | Usually more than than one path is available. Single point of failure are reduced by working as a group |
| Storage Requirements         | higher or same as reactive         | lower or same as proactive                         | depends on the size of the cluster or a zone                                                            |
| Delay level                  | small                              | might be higher than proactive                     | for local communication is small                                                                        |
| Scalability level            | usually up to 100 nodes            | source routing up to 100 nodes. Other scale higher | up to 1000 nodes                                                                                        |


### 32.Explain the main principles of Dynamic Source Routing protocol. 
#### Route discovery
- S wants to send to D, but does not know the route, source (S) initiates route discovery
- Node S floods Route Request (RREQ)
- Each node appends own identifier before forwarding (RREQ)


TODO(insert picture)

#### Route Maintenance
- node S can detect if route to D does not exists anymore
- Route maintenance is only used when S wants to send data to D
- Confirmation of the receipt
    - by MAC protocol (e.g. the link level acknowledgement frame defined by IEEE 802.11)
    - passive acknowledgement (a node overhears a transmission of the packet it has just forwarded )
    - acknowledgement from destination to source is requested
    - if no route C returns Route Error to A. A removes the broken link from its cache, uses alternative path or initiates Route Discovery procedure

#### Route Reply (RREP)
- In a bidirectional graph/setup
    - RREP is sent by reversing the route received by RREQ
        - RREP includes the route from RREQ
- In a unidirectional graph/setup
    - If D does know the way to S, use that route
    - Else initiate a RREQ form D to S, where the RREP is piggybacked in the RREQ

TODO(insert picture)

#### General
- Intermediate nodes use source route (destination route??) to determine where to forward packet
- Packet header grows with route length
- Each node caches new routes
- When S finds a route [S, E, F, J, D] to node D, it also learns the nodes in between (S -> F) (both in RREQ and RREP)
- Node may also learn routes by overhearing data

#### Cache
- When node S learns that a route to node D is broken, it uses another route from its local cache, if such a route to D exists in its cache.  Otherwise, node S initiates route discovery by sending a route request
- Node X on receiving a Route Request for some node D can send a Route Reply if node X knows a route to node D
- Uses
    - Speed up route discovery
    - Reduce propagation of route requests


#### Advantages/Disadvantages
- Advantages
    - Routes maintained only between nodes who need to communicate
        - reduces overhead of route maintenance
    - Route caching can further reduce route discovery overhead
    - A single route discovery may yield many routes to the destination, due to intermediate nodes replying from local caches
- Disadvantages
    - Packet header size grows with route length due to source routing
    - Flood of route requests may potentially reach all nodes in the network
    - Caches become stale/invalid because of the constant movement of nodes
        - Increases the time it takes to send something to the node (because multiple nodes might return stale/invalid caches)

