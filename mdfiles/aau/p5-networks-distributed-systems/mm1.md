# Time, synchronisation and ordering
# Notes from slides
## Introduction
Clocks are used in various ways in our daily lives, without us even knowing it.
Clock are constantly changing and calibrating itself, as to not come out of sync and to be as accurate as can be.

For distributed systems, it gets even harder as there is a certain need to know if which order events happen.
If our daily lives we would normally use time to indicate what happened first, but it is also possible to do so using the order of the events.
E.g. if Person A hits Person B, where after Person B hits Person A, the time does not matter but the cause of the action and the order in which they happen is important.

There is of course still cases where the time is required, and so it is still useful in distributed systems.
But for this to be useful, there needs to be a precision and or accuracy of this time.

- bounded precision: |$C\_i(t)-C\_k(t)| \leq \pi$
- bounded accuracy: |$C\_i(t)-t| \leq a$

These formulas specify, that we need the precision of our clock i and k, to be without the limits of $\pi$, whereas the accuracy needs to be within a predefiend limit $a$ with a offset of $t$[^1].

Methods to fix these problems can as follows:

- clock synchronisation protocols
- use of logical clock and ordering (close to the Person A/Person B case)
- not relying on time or order of distributed events

All this will be explored in the coming notes.

## Clock synchronisation protocols {#clock-sync-protocols}
### Notions of time
There are various notions of time, the most commonly used one in our day-to-day lives is *Coordinated Universal Time* (**UTC**).

- Based on atomic clocks
- Adjusted to astronomic time (leap seconds, etc.)
- Broadcasted via:
    - Radio (0.1 - 10ms accuracy)
    - GPS ($< 1\mu s$ accuracy)

There are other kinds of clocks as well, called internal clocks which are based on counters, that have a increment frequency (crystal).
Clocks in general drift over time, that is why they need to be kept in-sync.

### Clock sync I

```plantuml
participant "Server A" as srva
participant "Server B" as srvb

srva -> srvb: request server B time
srvb -> srva: Bs current timestamp
srva -> srva: sets time to <math>t_b+(max-min)/2</math>
note left: Server A knows bounds min,max\non the one-way communication delay
```

### Clock sync II (Christians algorithm)

```plantuml
participant "Server A" as srva
participant "Server B" as srvb

srva -> srvb: request server B send/receive timestamp
srvb -> srva: Bs send/receive timestamp
srva -> srva: sets time to <math>t_b+R/2</math>
note left: Server A estimates roundtrip time R
```

### Clock sync III (Berkeley algorithm)
Leader is elected at beginning, upon a failure a new leader is elected.

```plantuml
participant "Leader" as l
participant "Follower 1" as f1
participant "Follower 2" as f2
participant "Follower n" as fn

l -> f1: request time
note left: Leader polls all followers time\nsimilar to Christians algorithm
f1 -> l: send/receive timestamp
l -> f2: request time
f2 -> l: send/receive timestamp
l -> fn: request time
fn -> l: send/receive timestamp

l -> l: use RTT (round-trip time) to estimae\nfollowers clock
l -> l: average clock time and ignore any values\nwith large deviation from others
l -> f1: required adjustment
note left: adjustment is + or - integer,\nto avoid uncertainties due to RTT
l -> f2: required adjustment
l -> fn: required adjustment
```

### Clock sync IV (NTP)
UDP based protocol to distribute time information over the internet.
The goal of the protocol is to sync clients to UTC, and is based on a hierarchy of servers (strata - see graph).
The primary servers are connected to a UTC source.
If the primary server looses its UTC connection, it becomes secondary and a new server will be selected from the lower-stratum.

```plantuml
digraph test {
    1
    1 -> 2
    1 -> "2 "
    2 -> 3
    2 -> "3 "
}
```

#### Sync modes

- Multicast (within LAN, using delay assumptions)
- Client-Server
- Symmetric (peer-2-peer)



## Ordering and logical clocks

# Exam Questions
- Explain the need for clock synchronisation by giving an example scenario. Provide an example synchronisation approach for 2 nodes and discuss its advantages/disadvantages
- Explain the concept of causal partial order using an example event diagram.
- Explain the motivation and realization of a logical clock using an example.
- Explain vector clocks and scalar logical clocks using an example and discuss their differences


[^1]: Examples of accuracy and precision can be seen [here](https://en.wikipedia.org/wiki/Accuracy_and_precision)
